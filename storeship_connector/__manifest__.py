{
    'name': 'Storeship Connector',
    'version': '1.0',
    'author': '',
    'website': '',
    'license': 'AGPL-3',
    'category': 'sales',
    'summary': 'Storeship Connector',
    'depends': [
        'base',
        'sale',
        'sale_management',
        'product'
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/storeship_data.xml',
        'views/storeship_view.xml',
        'views/product_template_views.xml',
        'views/sale_view.xml'
    ],
    'installable': True,
}
