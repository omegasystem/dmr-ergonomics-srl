from odoo import fields, models, _


class SaleOrder(models.Model):
    _inherit = "sale.order"

    storeship_sale_id = fields.Char('Storeship Id')
    storeship_order_no = fields.Char('Storeship OrderNo')
    shipped_date = fields.Datetime('Shipped Date')
    sync_type = fields.Selection([
        ('draft', 'Draft'),
        ('sync', 'Synced')
    ], string='Sync Type', default='draft')
    order_tracking = fields.Char('order Tracking')
    order_trackUrl = fields.Char('order TrackUrl')
    shipping_status = fields.Char(string='Shipped Status')

    # @api.model
    # def create(self, vals):
    #     if vals.get('partner_id'):
    #         partner_id = self.env['res.partner'].browse(vals.get('partner_id'))
    #         if not partner_id.street or not partner_id.city or not partner_id.country_id or not partner_id.zip:
    #             raise ValidationError(_('Partner Address, Town, Country, Zip is required!'))
    #     res = super(SaleOrder, self).create(vals)
    #     company_id = self.env['res.company'].search([
    #         ('name', '=', 'DMR Ergonomics SRL (UK)')
    #     ])
    #     company_id.create_order(res)
    #     return res
