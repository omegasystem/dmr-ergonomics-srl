import urllib
import json
import logging
from odoo import fields, models, _

_logger = logging.getLogger(__name__)


class ResCompany(models.Model):
    _inherit = "res.company"

    last_sync_product = fields.Datetime('Last Sync Product')
    last_sync_orders = fields.Datetime('Last Sync Orders')
    location_id = fields.Many2one('stock.location', string='Location')
    product_error = fields.Text('Product Error')
    order_error = fields.Text('Order Error')
    single_error = fields.Text('Single Order Error')
    total_pending_order = fields.Integer(compute='_get_total_order', string='Pending Order')
    limit_order = fields.Integer('Limit Order')

    def _get_total_order(self):
        sale_orders = self.env['sale.order'].search_count([
            ('sync_type', '=', 'draft'),
        ])
        self.total_pending_order = sale_orders

    def create_order(self, res):
        # _logger.info('create_order--------------------------- ', res)
        res_list = []
        res_dict = {}
        if res:
            if res.is_custom or res.is_address_custom:
                if res.business_name:
                    if res.client_order_ref:
                        res_dict.update({'sName': res.business_name + " " + (res.client_order_ref)})
                    else:
                        res_dict.update({'sName': res.business_name})
                if res.partner_id and res.partner_id.company_id:
                    res_dict.update({'sCompany': ''})
                else:
                    res_dict.update({'sCompany': ''})
                if res.street:
                    res_dict.update({'sAdd1': res.street})
                if res.street2:
                    res_dict.update({'sAdd2': res.street2})
                if res.city:
                    res_dict.update({'sTown': res.city})
                if res.state_id:
                    res_dict.update({'sState': res.state_id.name})
                if res.partner_id.zip:
                    res_dict.update({'sPostcode': res.zip})
                if res.country_id:
                    res_dict.update({'sCountryCode': res.country_id.code})
                if res.custom_tel:
                    res_dict.update({'sPhone': res.custom_tel})
                res_list.append({'sEmail': ''})

            else:
                if res.partner_shipping_id:
                    if res.client_order_ref:
                        res_dict.update({'sName': res.partner_id.name})
                    else:
                        res_dict.update({'sName':  res.partner_id.name})
                    if res.partner_shipping_id and res.partner_shipping_id.company_id:
                        if res.client_order_ref:
                            res_dict.update(
                                {'sCompany': "%s (%s)"%(res.partner_shipping_id.company_id.name, res.client_order_ref)})
                        else:
                            res_dict.update({'sCompany': res.partner_shipping_id.company_id.name})
                    else:
                        if res.client_order_ref:
                            res_dict.update(
                                {'sCompany': "%s (%s)"%(res.partner_id.name, res.client_order_ref)})
                        else:
                            res_dict.update({'sCompany': res.partner_id.name})

                    if res.partner_shipping_id and res.partner_shipping_id.street:
                        res_dict.update(
                            {'sAdd1': res.partner_shipping_id.street})
                    if res.partner_shipping_id and res.partner_shipping_id.street2:
                        res_dict.update(
                            {'sAdd2': res.partner_shipping_id.street2})
                    if res.partner_shipping_id and res.partner_shipping_id.city:
                        res_dict.update(
                            {'sTown': res.partner_shipping_id.city})
                    if res.partner_shipping_id and res.partner_shipping_id.state_id:
                        res_dict.update(
                            {'sState': res.partner_shipping_id.state_id.name})
                    if res.partner_shipping_id and res.partner_shipping_id.zip:
                        res_dict.update(
                            {'sPostcode': res.partner_shipping_id.zip})
                    if res.partner_shipping_id and res.partner_shipping_id.country_id:
                        res_dict.update(
                            {'sCountryCode': res.partner_shipping_id.country_id.code})
                    if res.partner_shipping_id and res.partner_shipping_id.phone:
                        res_dict.update(
                            {'sPhone': res.partner_shipping_id.phone})
                    if res.partner_shipping_id and res.partner_shipping_id.email:
                        res_dict.update(
                            {'sEmail': ''})

                if not res.partner_shipping_id:
                    if res.client_order_ref:
                        res_dict.update(
                            {'sName': res.partner_id.display_name + " " + (res.client_order_ref)})
                    else:
                        res_dict.update(
                            {'sName': res.partner_id.display_name})
                    if res.partner_id and res.partner_id.company_id:
                        if res.client_order_ref:
                            res_dict.update(
                                {'sCompany': "%s (%s)"%(res.partner_id.company_id.name, res.client_order_ref)})
                        else:
                            res_dict.update({'sCompany': res.partner_id.company_id.name})
                    else:
                        if res.client_order_ref:
                            res_dict.update(
                                {'sCompany': "%s (%s)"%(res.partner_id.name, res.client_order_ref)})
                        else:
                            res_dict.update({'sCompany': res.partner_id.name})
                    if res.partner_id and res.partner_id.street_name:
                        res_dict.update(
                            {'sAdd1': res.partner_id.street_name})

                    if res.partner_id and res.partner_id.street2:
                        res_dict.update(
                            {'sAdd2': res.partner_id.street2})

                    if res.partner_id and res.partner_id.city:
                        res_dict.update(
                            {'sTown': res.partner_id.city})

                    if res.partner_id and res.partner_id.state_id:
                        res_dict.update(
                            {'sState': res.partner_id.state_id.name})

                    if res.partner_id and res.partner_id.zip:
                        res_dict.update(
                            {'sPostcode': res.partner_id.zip})

                    if res.partner_id and res.partner_id.country_id:
                        res_dict.update(
                            {'sCountryCode': res.partner_id.country_id.code})

                    if res.partner_id and res.partner_id.phone:
                        res_dict.update(
                            {'sPhone': res.partner_id.phone})

                    if res.partner_id and res.partner_id.email:
                        res_dict.update(
                            {'sEmail': ''})

            if res.name:
                res_dict.update(
                    {'oOrderNo': str(res.name)})
            if res.client_order_ref:
                res_dict.update(
                    {'oReference': str(res.client_order_ref)})
            else:
                res_dict.update(
                    {'oReference': str(res.name)})
            if res.download_file:
                res_dict.update(
                    {'oLink': res.download_file})
                res_list.append({'oLink': res.download_file})
            else:
                res_dict.update(
                    {'oLink': ''})
            res_dict.update({
                'oValue': '',
                'oNotes': res.shipstore_note,
                'oShipOption': res.ship_option_id.ship_id
            })

            if res.order_line:
                order_lines = []
                for line in res.order_line:
                    if line.product_id.type == 'product':
                        order_lines.append({
                            'Name': line.product_id.name,
                            'SKU': line.product_id.default_code,
                            'Quantity': line.product_uom_qty
                        })
                res_dict.update({'order_lines': order_lines})
        final_dict = {'datas': res_dict}
        print("json_data--------------------", final_dict)
        path = 'http://www.standivarius.com/storeship/test.php'
        parse_data = urllib.parse.urlencode(res_dict).encode("utf-8")
        req = urllib.request.Request(path, parse_data)
        page = urllib.request.urlopen(req).read()
        my_json = page.decode('utf8').replace("'", '"')
        print("my_json--------------------", my_json)
        _logger.info('my_json--------------------------- ', my_json)
        res.write({'log': my_json})
        try:
            data = json.loads(my_json)
            print("data--------------------", data)
            if 'status' in data:
                if data.get('status') == 200:
                    order_value = data.get('status_message')
                    word_list = order_value.split()
                    _logger.info('word_list %s', word_list)
                    res.write({'storeship_sale_id': word_list[2]})
            else:
                res.write({'log': data})
        except ValueError:
            return

