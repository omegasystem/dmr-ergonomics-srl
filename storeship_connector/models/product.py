from odoo import fields, models


class ProductTemplate(models.Model):
    _inherit = "product.template"

    storeship_id = fields.Char('Storeship Id')
    productSSBarcode = fields.Char('SS Barcode')
    product_track_ids = fields.One2many('storeship.product.track', 'product_id', string='StoreShip Updates')


class StorShipProductTrack(models.Model):
    _name = "storeship.product.track"

    product_id = fields.Many2one('product.template', string="Product Id")
    sync_date = fields.Datetime('Last Update')
    qty = fields.Float('Updated Qty')
