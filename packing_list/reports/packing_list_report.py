# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class PackingListReport(models.AbstractModel):
    _name = 'report.packing_list.report_packing_list'

    @api.model
    def _get_report_values(self, docids, data=None):
        print("cal-----------------", docids, data)
        packing_list_report = self.env['ir.actions.report']._get_report_from_name('packing_list.report_packing_list')
        packing_lists = self.env['sale.order'].browse(docids)
        return {
            'doc_ids': self.ids,
            'doc_model': packing_list_report.model,
            'docs': packing_lists,
        }
