# -*- coding: utf-8 -*-

import os
import ssl
import ftplib
from ftplib import FTP_TLS,FTP

from odoo import models, fields, api, tools, _
from odoo.exceptions import Warning, AccessDenied
import odoo

import logging
_logger = logging.getLogger(__name__)

try:
    import paramiko
except ImportError:
    raise ImportError(
        'This module needs paramiko to automatically write backups to the FTP through SFTP. '
        'Please install paramiko on your system. (sudo pip3 install paramiko)')


# server: <ftps://www243.your-server.de>
# user: meinva_2
# pass: te2cptZyBezJqi4j

class ImplicitFTP_TLS(ftplib.FTP_TLS):
    """FTP_TLS subclass that automatically wraps sockets in SSL to support implicit FTPS."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._sock = None

    @property
    def sock(self):
        """Return the socket."""
        return self._sock

    @sock.setter
    def sock(self, value):
        """When modifying the socket, ensure that it is ssl wrapped."""
        if value is not None and not isinstance(value, ssl.SSLSocket):
            value = self.context.wrap_socket(value)
        self._sock = value

class OdooFtp(models.Model):
    _name = 'odoo.ftp'
    _description = 'Backup configuration record'

    # Columns for local server configuration
    type = fields.Selection([
        ('ftp', 'FTP'),
        ('sftp', 'SFTP')
    ], string='Type')
    host = fields.Char('Host', required=True, default='localhost')
    port = fields.Char('Port', required=True, default=8069)
    name = fields.Char('File name', required=True)
    folder = fields.Char('File Directory')

    # Columns for external server (SFTP)
    sftp_write = fields.Boolean('Write to external server with sftp',
                                help="If you check this option you can specify the details needed to write to a remote "
                                     "server with SFTP.")
    sftp_path = fields.Char('Path external server',
                            help='The location to the folder where the dumps should be written to. For example '
                                 '/odoo/backups/.\nFiles will then be written to /odoo/backups/ on your remote server.')
    sftp_host = fields.Char('IP Address SFTP Server',
                            help='The IP address from your remote server. For example 192.168.0.1')
    sftp_port = fields.Integer('SFTP Port', help='The port on the FTP server that accepts SSH/SFTP calls.', default=22)
    sftp_user = fields.Char('Username SFTP Server',
                            help='The username where the SFTP connection should be made with. This is the user on the '
                                 'external server.')
    sftp_password = fields.Char('Password User SFTP Server',
                                help='The password from the user where the SFTP connection should be made with. This '
                                     'is the password from the user on the external server.')
    days_to_keep_sftp = fields.Integer('Remove SFTP after x days',
                                       help='Choose after how many days the backup should be deleted from the FTP '
                                            'server. For example:\nIf you fill in 5 the backups will be removed after '
                                            '5 days from the FTP server.',
                                       default=30)
    send_mail_sftp_fail = fields.Boolean('Auto. E-mail on backup fail',
                                         help='If you check this option you can choose to automaticly get e-mailed '
                                              'when the backup to the external server failed.')
    email_to_notify = fields.Char('E-mail to notify',
                                  help='Fill in the e-mail where you want to be notified that the backup failed on '
                                       'the FTP.')

    def test_sftp_connection(self, context=None):
        self.ensure_one()

        # Check if there is a success or fail and write messages
        message_title = ""
        message_content = ""
        error = ""
        has_failed = False

        for rec in self:
            path_to_write_to = rec.sftp_path
            ip_host = rec.sftp_host
            port_host = rec.sftp_port
            username_login = rec.sftp_user
            password_login = rec.sftp_password

            # Connect with external server over SFTP, so we know sure that everything works.
            try:
                s = paramiko.SSHClient()
                s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                s.connect(ip_host, port_host, username_login, password_login, timeout=10)
                sftp = s.open_sftp()
                message_title = _("Connection Test Succeeded!\nEverything seems properly set up for FTP back-ups!")
            except Exception as e:
                _logger.critical('There was a problem connecting to the remote ftp: ' + str(e))
                error += str(e)
                has_failed = True
                message_title = _("Connection Test Failed!")
                if len(rec.sftp_host) < 8:
                    message_content += "\nYour IP address seems to be too short.\n"
                message_content += _("Here is what we got instead:\n")
            finally:
                if s:
                    s.close()

        if has_failed:
            raise Warning(message_title + '\n\n' + message_content + "%s" % str(error))
        else:
            raise Warning(message_title + '\n\n' + message_content)

    # @api.model
    def schedule_backup(self):
        # conf_ids = self.search([])
        for rec in self:
            # Check if user wants to write to SFTP or not.
            if rec.sftp_write is True:
                if rec.type == 'sftp':
                    path_to_write_to = self.sftp_path
                    s = paramiko.SSHClient()
                    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                    s.connect(self.sftp_host, 22, self.sftp_user, self.sftp_password,
                              timeout=20)
                    sftp = s.open_sftp()
                    try:
                        sftp.chdir(path_to_write_to)
                    except IOError:
                        current_directory = ''
                        for dirElement in path_to_write_to.split('/'):
                            current_directory += dirElement + '/'
                            try:
                                sftp.chdir(current_directory)
                            except:
                                _logger.info(
                                    '(Part of the) path didn\'t exist. Creating it now at ' + current_directory)
                                # Make directory and then navigate into it
                                sftp.mkdir(current_directory, 777)
                                sftp.chdir(current_directory)
                                pass
                    print("path_to_write_to-------", path_to_write_to)
                    sftp.chdir("/"+path_to_write_to)
                    file_name = "/"+ path_to_write_to + "/StandivariusUKStock.csv"
                    try:
                        sftp.put(str(self.folder), file_name)
                        _logger.info(
                            'Copying File ------ success')
                    except Exception as err:
                        _logger.critical(
                            'We couldn\'t write the file to the remote server. Error: ' + str(
                                err))
                    sftp.close()
                    s.close()
                else:
                    # ftp_server = ftplib.FTP(rec.sftp_host, rec.sftp_user, rec.sftp_password)
                    # ftp_server.encoding = "utf-8"
                    # print("session-------------", ftp_server.dir())
                    # with open(rec.folder, 'rb') as file:  # file to send
                    #     print("file-------------", file)
                    #     ftp_server.storbinary(f"STOR {rec.name}", file)  # send the file
                    # file.close()  # close file and FTP
                    # ftp_server.quit()
                    ftp = FTP(rec.sftp_host, timeout=2)
                    ftp.login(rec.sftp_user, rec.sftp_password)
                    with open(rec.folder, 'rb') as file:  # file to send
                        print("file-------------", file)
                        ftp.storbinary(f"STOR {rec.name}", file)  # send the file
                    ftp.quit()

    def generate_proshop_file(self):
        # conf_ids = self.search([])
        for rec in self:
            # Check if user wants to write to SFTP or not.
            if rec.sftp_write is True:
                if rec.type == 'sftp':
                    path_to_write_to = self.sftp_path
                    s = paramiko.SSHClient()
                    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                    s.connect(self.sftp_host, 22, self.sftp_user, self.sftp_password,
                              timeout=20)
                    sftp = s.open_sftp()
                    try:
                        sftp.chdir(path_to_write_to)
                    except IOError:
                        current_directory = ''
                        for dirElement in path_to_write_to.split('/'):
                            current_directory += dirElement + '/'
                            try:
                                sftp.chdir(current_directory)
                            except:
                                _logger.info(
                                    '(Part of the) path didn\'t exist. Creating it now at ' + current_directory)
                                # Make directory and then navigate into it
                                sftp.mkdir(current_directory, 777)
                                sftp.chdir(current_directory)
                                pass
                    print("path_to_write_to-------", path_to_write_to)
                    sftp.chdir("/"+path_to_write_to)
                    file_name = "/"+ path_to_write_to + "/proshop.csv"
                    try:
                        sftp.put(str(self.folder), file_name)
                        _logger.info(
                            'Copying File ------ success')
                    except Exception as err:
                        _logger.critical(
                            'We couldn\'t write the file to the remote server. Error: ' + str(
                                err))
                    sftp.close()
                    s.close()
                else:
                    # ftp_server = ftplib.FTP(rec.sftp_host, rec.sftp_user, rec.sftp_password)
                    # ftp_server.encoding = "utf-8"
                    # print("session-------------", ftp_server.dir())
                    # with open(rec.folder, 'rb') as file:  # file to send
                    #     print("file-------------", file)
                    #     ftp_server.storbinary(f"STOR {rec.name}", file)  # send the file
                    # file.close()  # close file and FTP
                    # ftp_server.quit()
                    ftp = FTP(rec.sftp_host, timeout=2)
                    ftp.login(rec.sftp_user, rec.sftp_password)
                    with open(rec.folder, 'rb') as file:  # file to send
                        print("file-------------", file)
                        ftp.storbinary(f"STOR {rec.name}", file)  # send the file
                    ftp.quit()



