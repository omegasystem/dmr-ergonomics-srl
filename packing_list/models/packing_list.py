from odoo import models, fields, api
from odoo.tools import unique
import xlsxwriter
import base64
import csv
import logging
_logger = logging.getLogger(__name__)


class StockMove(models.Model):
    _inherit = 'stock.move'

    pack_number = fields.Integer(string="Package Number")
    remarks = fields.Char('Remarks')


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    product_ref = fields.Boolean(string="Product Reference")
    total_package = fields.Integer(compute="_total_packages", string="Packages")

    def print_report(self):
        records = self.env['stock.picking'].search([('id', '=', self.id)])
        if records:
            return self.env.ref('packing_list.action_packing_list_report').report_action(records)

    def _total_packages(self):
        rec = self.env['stock.move'].search([('picking_id', '=', self.id)]).mapped('pack_number')
        test = list(unique(rec))
        self.total_package = len(test)

    def generate_csv(self):
        file_path = 'StandivariusUKStock.csv'
        workbook = xlsxwriter.Workbook('/tmp/' + file_path)
        exist_attachment = self.env['ir.attachment'].sudo().search(
            [('name', '=', 'StandivariusUKStock.csv')]
        )
        if exist_attachment:
            exist_attachment.unlink()
        with open('/tmp/StandivariusUKStock.csv', 'w',
                  newline='') as csvFile:
            writer = csv.writer(csvFile)
            TITLEHEDER = ['SKU', 'QTY']
            stock_location_id = self.env['stock.location'].search([
                ('is_storeship', '=', True)
            ])
            products = self.env['product.product'].search([
                ('remove_csv', '=', False)
            ])
            writer.writerow(TITLEHEDER)
            for product in products:
                if product.default_code and product.default_code.startswith('ST'):
                    stock_data = self.env['stock.quant'].search([
                        ('location_id', '=', stock_location_id.id),
                        ('product_id', '=', product.id)
                    ])
                    if stock_data.quantity > 0:
                        product_name = [
                            str(stock_data.product_id.default_code),
                            str(int(stock_data.quantity)),
                        ]
                        writer.writerow(product_name)
                    else:
                        product_name = [
                            str(product.default_code),
                            str(0),
                        ]
                        writer.writerow(product_name)

            csvFile.close()
            files = base64.b64encode(
                open('/tmp/StandivariusUKStock.csv', 'rb+').read())

    def generate_csv2(self):
        files = base64.b64encode(
            open('/tmp/StandivariusUKStock.csv', 'rb+').read())
        exist_attachment = self.env['ir.attachment'].sudo().search([
            ('name', '=', 'StandivariusUKStock.csv')
        ])
        if exist_attachment:
            exist_attachment.unlink()
        attachment = self.env['ir.attachment'].sudo().create({
            'name': 'StandivariusUKStock.csv',
            'datas': files,
        })
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        download_url = '/web/content/' + str(
            attachment.id) + '?download=true'
        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + str(download_url),
            "target": "new",
        }

    def generate_proshop_csv(self):
        file_path = 'proshop.csv'
        workbook = xlsxwriter.Workbook('/tmp/' + file_path)
        exist_attachment = self.env['ir.attachment'].sudo().search(
            [('name', '=', 'proshop.csv')]
        )
        if exist_attachment:
            exist_attachment.unlink()
        with open('/tmp/proshop.csv', 'w',
                  newline='') as csvFile:
            writer = csv.writer(csvFile)
            TITLEHEDER = ['PartNo', 'ArticleNo', 'Name', 'Price', 'Stock', 'Brand', 'Weight', 'EAN', 'Category', 'Subcategory']
            stock_location_id = self.env['stock.location'].search([
                ('id', '=', 113)
            ])
            price_list = self.env['product.pricelist'].search([
                ('name', '=', 'EUR-ProShop')
            ])
            writer.writerow(TITLEHEDER)
            for product in price_list.item_ids:
                stock_data = self.env['stock.quant'].search([
                    ('location_id', '=', stock_location_id.id),
                    ('product_id', '=', product.product_tmpl_id.product_variant_id.id)
                ])
                if stock_data.quantity > 0:
                    product_name = [
                        str(product.product_tmpl_id.default_code),
                        str(product.product_tmpl_id.default_code),
                        str(product.product_tmpl_id.name),
                        str(product.fixed_price),
                        str(int(stock_data.quantity)),
                        'standivarius',
                        str(product.product_tmpl_id.weight),
                        str(product.product_tmpl_id.barcode),
                        'Kontor – og hobbyartikler',
                        'Ergonomi'
                    ]
                    writer.writerow(product_name)
                else:
                    product_name = [
                        str(product.product_tmpl_id.default_code),
                        str(product.product_tmpl_id.default_code),
                        str(product.product_tmpl_id.name),
                        str(product.fixed_price),
                        str(0),
                        'standivarius',
                        str(product.product_tmpl_id.weight),
                        str(product.product_tmpl_id.barcode),
                        'Kontor – og hobbyartikler',
                        'Ergonomi'
                    ]

                    writer.writerow(product_name)
                _logger.info('product_name File % s------ success' % product_name)
            csvFile.close()
            files = base64.b64encode(
                open('/tmp/proshop.csv', 'rb+').read())


    def generate_proshop_csv2(self):
        files = base64.b64encode(
            open('/tmp/proshop.csv', 'rb+').read())
        exist_attachment = self.env['ir.attachment'].sudo().search([
            ('name', '=', 'proshop.csv')
        ])
        if exist_attachment:
            exist_attachment.unlink()
        attachment = self.env['ir.attachment'].sudo().create({
            'name': 'proshop.csv',
            'datas': files,
        })
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        download_url = '/web/content/' + str(
            attachment.id) + '?download=true'
        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + str(download_url),
            "target": "new",
        }


