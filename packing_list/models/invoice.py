from odoo import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    _inherit = 'account.move'

    client_order_ref = fields.Char('client_order_ref')
    orderTrackUrl = fields.Char('orderTrackUrl')

    @api.model
    def create(self, vals):
        _logger.info("invoice=--------================")
        res = super(AccountMove, self).create(vals)
        sale_id = self.env['sale.order'].search([
            ('name', '=', res.invoice_origin)
        ])
        if sale_id:
            _logger.info("sale_id.warehouse_id.code=--------================", sale_id.warehouse_id)
            if sale_id.warehouse_id.code in ['STS', 'ST-UK']:
                company_id = self.env['res.company'].search([
                    ('name', '=', 'DMR Ergonomics SRL (UK)')
                ])
                _logger.info("company_id=--------================", company_id)
                company_id.create_order(sale_id)
        return res


