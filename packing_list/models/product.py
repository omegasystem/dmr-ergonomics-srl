import urllib
# import re
import ssl
import json
from odoo import models, fields, api, _

import logging
_logger = logging.getLogger(__name__)


class ProductProduct(models.Model):
    _inherit = 'product.product'

    package = fields.Integer(
        'Package', default=1
    )
    bundle_product = fields.Many2one('product.product', string='Bundle Product')
    display_users = fields.Many2many('res.users',string='Users')
    show_in_portal = fields.Boolean('Show in Portal', default=True)
    storeship_qty = fields.Float('StoreShip QTY')
    product_url = fields.Char('Product URL')
    remove_csv = fields.Boolean('Remove CSV')

    def scheduled_get_product(self):
        path = 'http://www.standivarius.com/storeship/get_products.php'
        data = [('store_data', 'record.storeship_sale_id')]
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        parse_data = urllib.parse.urlencode(data).encode("utf-8")
        req = urllib.request.Request(path, parse_data)
        req.add_header("Content-type", "application/x-www-form-urlencoded")
        page = urllib.request.urlopen(req).read()
        my_json = page.decode('utf8').replace("'", '"')
        data = json.loads(my_json)
        # print("data------------------", data)
        _logger.info("data=--------================:%s" % data)
        if data.get('status') == 200:
            product_value = data.get('data')
            _logger.info("product_value=--------================:%s" % product_value)
            location_id = self.env['stock.location'].sudo().search([
                ('is_storeship', '=', True)
            ])
            # matches = ['Bundle', 'bundle']
            line_ids = []
            company_id = self.env['res.company'].search([
                ('name', '=', 'DMR Ergonomics SRL (UK)')
            ])
            product_obj = self.env['product.product']
            for product in product_value:
                line_dict = {}
                # bundle_value = 1.0
                # if any(x in product.get('productName') for x in matches):
                #     temp = re.findall(r'\d+', product.get('productName'))
                #     if temp:
                #         bundle_value = temp[0]
                product_id = product_obj.search(['|',
                    ('default_code', '=', product.get('productSKU')),
                    ('barcode', '=', product.get('productBarcode')),
                ], limit=1)
                # print("product_id----------------", product_id)
                _logger.info("product_id=--------================:%s" % product_id)
                if product_id:
                    product_id.write({'storeship_qty': float(product.get('productStock'))})
                    line_dict.update({
                        'product_id': product_id.id,
                        'product_uom_id': product_id.uom_id.id,
                        'product_qty': float(product.get('productStock')),
                        'location_id': location_id.id
                    })
                    line_ids.append((0, 0, line_dict))
            location_id.write({'last_sync': fields.Datetime.now()})
            inventory = self.env['stock.inventory'].create({
                'name': 'Initial inventory',
                'line_ids': line_ids,
                'company_id': company_id.id
            })
            inventory.action_start()
            inventory.action_validate()
