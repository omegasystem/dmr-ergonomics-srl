from odoo import models, fields, api, _


class ProductPriceList(models.Model):
    _inherit = 'product.pricelist'

    description = fields.Text('Description')


class ProductPriceListItem(models.Model):
    _inherit = 'product.pricelist.item'

    rrp = fields.Text('RRP')
