import urllib
import json
import ssl
import requests
from odoo import models, fields, api, _

import logging
_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    send_email = fields.Boolean('Send Email')
    ref_email = fields.Char('Ref Email')
    pricelist_id = fields.Many2one('product.pricelist',
                                   string='PriceList')

    @api.onchange('property_product_pricelist')
    def _onchange_property_product_pricelist(self):
        if self.property_product_pricelist:
            self.pricelist_id = self.property_product_pricelist.id


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    # @api.onchange('product_id')
    # def onchange_product_id(self):
    #     _logger.info("onchange_product_id=--------================", self.order_id.warehouse_id)
    #     if self.product_id:
    #         if self.order_id.warehouse_id and self.order_id.warehouse_id.code in ['STS', 'ST-UK']:
                # location_id = self.env['stock.location'].sudo().search([
                #     ('is_storeship', '=', True)
                # ], limit=1)
                # _logger.info("location_id=--------================:%s" % self.order_id.warehouse_id.lot_stock_id)
                # qty_available = self.env['stock.quant']._get_available_quantity(self.product_id, self.order_id.warehouse_id.lot_stock_id)
                # _logger.info("qty_available=--------================:%s" % qty_available)
                #
                # if qty_available <= 0.0:
                #     location_name = "No qty available in " + self.order_id.warehouse_id.name + "!"
                #     raise UserError(_(location_name))


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    custom_customer_name = fields.Char('Name')
    custom_address_id = fields.Many2one('res.partner', string='Custom Address')
    is_custom = fields.Boolean('Is Custom?')
    attach_delivery_list = fields.Binary('Delivery List', attachment=True)
    binary_fname = fields.Char('Binary Name')
    download_file = fields.Char(string='Download URL')
    street = fields.Char('Street')
    street2 = fields.Char('Street2')
    city = fields.Char('City')
    state_id = fields.Many2one('res.country.state', string='State')
    country_id = fields.Many2one('res.country', string='Country')
    zip = fields.Char('Zip')
    custom_tel = fields.Char('Tel')
    shipstore_note = fields.Text('Shipstore Note')
    ship_option_id = fields.Many2one('ship.option', string='Ship Option')
    log = fields.Text("Log")

    def resync_record(self):
        if self.warehouse_id.code in ['STS', 'ST-UK']:
            company_id = self.env['res.company'].search([
                ('name', '=', 'DMR Ergonomics SRL (UK)')
            ])
            company_id.create_order(self)

    @api.model
    def create(self, vals):
        res = super(SaleOrder, self).create(vals)
        base_url = "https://odoo.standivarius.com"
        if vals.get('attach_delivery_list'):
            url = base_url + '/demo/content?model=sale.order&field=attach_delivery_list&filename_field=binary_fname&id=%s' % (
                res.id)
        else:
            url = base_url + '/packing/' + str(res.id)
        res.download_file = url
        return res

    def save_to_file(self):
        return {
             'type' : 'ir.actions.act_url',
             'url': self.download_file,
             'target': 'new',
        }  
    # def write(self, vals):
    #     print("write----------------", vals)
    #     base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
    #     if vals.get('attach_delivery_list'):
    #         url = base_url + '/demo/content?model=sale.order&field=attach_delivery_list&filename_field=binary_fname&id=%s' % (self.id)
    #     else:
    #         url = base_url + '/packing/' + str(self.id)
    #     vals.update({'download_file': url})
    #     res = super(SaleOrder, self).write(vals)
    #     return res

    def scheduled_next_date(self):
        records = self.env['sale.order'].search([
            ('storeship_sale_id', '!=', False),
            ('sync_type', '=', 'draft'),
            ('state', '=', 'sale')
        ])
        path = 'http://www.standivarius.com/storeship/fetch_order.php'
        schedule_email_obj = self.env['schedule.email']
        for record in records:
            res_dict = {'store_data': str(record.storeship_sale_id)}
            ctx = ssl.create_default_context()
            ctx.check_hostname = False
            ctx.verify_mode = ssl.CERT_NONE
            parse_data = urllib.parse.urlencode(res_dict).encode("utf-8")
            req = urllib.request.Request(path, parse_data)
            req.add_header("Content-type", "application/x-www-form-urlencoded")
            page = urllib.request.urlopen(req, context=ctx).read()
            my_json = page.decode('utf8').replace("'", '"')
            try:
                data = json.loads(my_json)
                update_dict = {}
                send_email = False
                if data.get('status') == 200:
                    order_value = data.get('data')
                    print("order_value------------------------", order_value)
                    if order_value.get('orderTracking'):
                        update_dict.update({'order_tracking': order_value.get('orderTracking')})
                        update_dict.update({'sync_type': 'sync'})
                        send_email = True
                    if order_value.get('orderTrackUrl'):
                        update_dict.update({'order_trackUrl': order_value.get('orderTrackUrl')})
                    if order_value.get('orderShipped'):
                        update_dict.update({'shipping_status': order_value.get('orderShipped')})
                    record.write(update_dict)

                if send_email:
                    if record.partner_id.send_email and record.invoice_ids:
                        invoice = record.invoice_ids.sorted(key=lambda inv: inv.id, reverse=False)[-1]
                        if record.client_order_ref:
                            invoice.write({
                                'client_order_ref': record.client_order_ref,
                                'orderTrackUrl': order_value.get('orderTrackUrl')
                            })
                        schedule_email = schedule_email_obj.search([
                            ('invoice_id', '=', invoice.id)
                        ])
                        if not schedule_email:
                            self.env['schedule.email'].create(
                                {'invoice_id': invoice.id, 'sale_id': record.id, 'in_draft': True})
                    else:
                        display_msg = """
                                        <b>Send Email Checkbox is not checked..</b><br/>
                                    """
                        record.message_post(body=display_msg)
                self.env.cr.commit()
            except:
                pass
            # my_json = page.decode('utf8').replace("'", '"')
            # _logger.info('my_json--------------------------- ', my_json)
            # return
            # data = json.loads(my_json)

    def scheduled_next_date_2(self):
        print("scheduled_next_date_2---------------------")
        path = 'http://www.standivarius.com/storeship/fetch_order.php'
        schedule_email_obj = self.env['schedule.email']
        for record in self:
            res_dict = {'store_data': str(record.storeship_sale_id)}
            ctx = ssl.create_default_context()
            ctx.check_hostname = False
            ctx.verify_mode = ssl.CERT_NONE
            parse_data = urllib.parse.urlencode(res_dict).encode("utf-8")
            req = urllib.request.Request(path, parse_data)
            req.add_header("Content-type", "application/x-www-form-urlencoded")
            page = urllib.request.urlopen(req,context=ctx).read()
            my_json = page.decode('utf8').replace("'", '"')
            print("me-----------",my_json)
            data = json.loads(my_json)
            update_dict = {}
            send_email = False
            if data.get('status') == 200:
                order_value = data.get('data')
                print("order_value---------------------%s---", order_value)
                if order_value.get('orderTracking'):
                    update_dict.update({'order_tracking': order_value.get('orderTracking')})
                    update_dict.update({'sync_type': 'sync'})
                    send_email = True
                if order_value.get('orderTrackUrl'):
                    update_dict.update({'order_trackUrl': order_value.get('orderTrackUrl')})
                if order_value.get('orderShipped'):
                    update_dict.update({'shipping_status': order_value.get('orderShipped')})
                record.write(update_dict)

            if send_email:
                if record.partner_id.send_email and record.invoice_ids:
                    invoice = record.invoice_ids.sorted(key=lambda inv: inv.id, reverse=False)[-1]
                    if record.client_order_ref:
                        invoice.write({
                            'client_order_ref': record.client_order_ref,
                            'orderTrackUrl': order_value.get('orderTrackUrl')
                        })
                    schedule_email = schedule_email_obj.search([
                        ('invoice_id', '=', invoice.id)
                    ])
                    if not schedule_email:
                        self.env['schedule.email'].create(
                            {'invoice_id': invoice.id, 'sale_id': record.id, 'in_draft': True})


class ShipOption(models.Model):
    _name = 'ship.option'

    name = fields.Char('Ship Name')
    ship_id = fields.Char('Ship Id')
