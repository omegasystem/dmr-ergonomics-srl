from odoo import models, fields, api, _


class StockLocation(models.Model):
    _inherit = 'stock.location'

    is_storeship = fields.Boolean('Is Storeship')
    last_sync = fields.Datetime('Last Sync')

class StockQuant(models.Model):
    _inherit = 'stock.quant'

    storeship_qty = fields.Float(compute='_get_storeship_qty', string='Storeship', store=True)

    def _get_storeship_qty(self):
        stock_location_id = self.env['stock.location'].search([
            ('is_storeship', '=', True)
        ])
        for record in self:
            stock_data = self.env['stock.quant'].search([
                ('location_id', '=', stock_location_id.id),
                ('product_id', '=', record.product_id.id)
            ])
            if stock_data.quantity > 0:
                record.storeship_qty = stock_data.quantity
            else:
                record.storeship_qty = 0.0