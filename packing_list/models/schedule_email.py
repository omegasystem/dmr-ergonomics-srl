from odoo import models, fields, api, _

import logging

_logger = logging.getLogger(__name__)


class ScheduleEmail(models.Model):
    _name = 'schedule.email'

    invoice_id = fields.Many2one('account.move', string='Invoice')
    sale_id = fields.Many2one('sale.order', string='Sale')
    in_draft = fields.Boolean('In Draft', default=False)

    def schedule_email(self):
        records = self.env['schedule.email'].search([
            ('in_draft', '=', True),
        ])
        for record in records:
            if record.sale_id.partner_id.ref_email:
                _logger.info("email=--------================:%s" %record.sale_id.partner_id.ref_email)
                email_values = {
                    'email_to': record.sale_id.partner_id.ref_email
                }
                template_storeship_tracking = self.env.ref(
                    'packing_list.email_template_send_storeship_tracking',
                    raise_if_not_found=False)
                template_storeship_tracking.sudo().send_mail(
                    record.invoice_id.id,
                    notif_layout='mail.mail_notification_light',
                    email_values=email_values,
                    force_send=True
                )
                record.in_draft = False
                display_msg = """
                    <b>Email Send to: """+record.sale_id.partner_id.ref_email+"""</b><br/>
                """
                record.sale_id.message_post(body=display_msg)
            else:
                _logger.info("email=--------=no===============:")
                display_msg = """
                                        <b>Ref Email not available..</b><br/>
                                    """
                record.sale_id.message_post(body=display_msg)
