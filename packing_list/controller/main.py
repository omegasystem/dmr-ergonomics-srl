# -*- coding: utf-8 -*-
from odoo.http import request
from odoo import http, _
import csv
import base64
import csv
from odoo import _, SUPERUSER_ID
from odoo.addons.web.controllers.main import Binary
from odoo.http import content_disposition, Controller, request, route
from odoo.addons.portal.controllers.portal import CustomerPortal
import logging

_logger = logging.getLogger(__name__)


class PackingController(http.Controller):

    @http.route(['/packing/<int:id>'], type='http', auth="public")
    def packing_content(self, id=None, access_token=None):
        pdf, _ = request.env.ref(
            'packing_list.action_packing_list_report').sudo().render_qweb_pdf(
            [id])
        pdfhttpheaders = [('Content-Type', 'application/pdf'),
                          ('Content-Length', u'%s' % len(pdf))]
        return request.make_response(pdf, headers=pdfhttpheaders)

    def get_products(self, product_item):
        products_list = []
        domain = []
        product_temp_obj = request.env['product.template']
        product_obj = request.env['product.product']
        if product_item.applied_on == '3_global':
            products = product_temp_obj.sudo().search([])
        if product_item.applied_on == '2_product_category':
            products = product_temp_obj.sudo().search([('categ_id', '=', product_item.categ_id.id)])
        if product_item.applied_on == '1_product':
            products = product_temp_obj.sudo().browse(product_item.product_tmpl_id.id)
        if product_item.applied_on == '0_product_variant':
            products = product_obj.sudo().browse(product_item.product_id.id)
        if products:
            for product in products:
                product_dict = {}
                if product_item.compute_price == 'fixed':
                    product_dict.update({'price': product_item.fixed_price})
                if product_item.compute_price == 'percentage':
                    dis_price = (product.lst_price * product_item.percent_price) / 100
                    product_dict.update({'price': product.lst_price - dis_price})

                product_dict.update({
                    'product_name': product.name,
                })
                products_list.append(product_dict)
        return products_list

    @http.route(['/price_list/<int:id>'], type='http', auth="user",
                website=True)
    def price_list(self, id=None, access_token=None):
        print("user-----------", request.env.user.partner_id)
        price_list_id = request.env.user.partner_id.pricelist_id
        price_list = request.env['product.pricelist'].sudo().browse(price_list_id.id)
        products_list = []
        if price_list:
            for product_item in price_list.item_ids:
                _logger.info(
                    "product_item=--------================:%s-----" % product_item)
                if product_item.compute_price == 'fixed' and \
                        product_item.fixed_price > 0.0:
                    products_list += self.get_products(product_item)
                if product_item.compute_price == 'percentage' and \
                        product_item.percent_price > 0.0:
                    products_list += self.get_products(product_item)
        _logger.info(
            "products_list=--------================:%s-----" % products_list)
        values = {
            'products': products_list,
        }
        print("values===========", values)
        return request.render("packing_list.pricelist_portal_products", values)

    @http.route('/subscribe_user_email', type='http', auth='public')
    def subscribe_user_email(self, send_email=None, email=None,
                             access_token=None):
        if send_email and email:
            request.env.user.partner_id.sudo().write({
                'send_email': True,
                'ref_email': email
            })
        else:
            request.env.user.partner_id.sudo().write({
                'send_email': False,
                'ref_email': ''
            })
        return

    @http.route(['/uk-stock2'], type='http', auth='public')
    def ExportStoreship2(self, **kw):
        request.env['stock.picking'].sudo().generate_csv2()
        return

    @http.route(['/uk-stock'], type='http', auth="user", website=True)
    def ExportStoreship(self, **kw):
        values ={}
        return request.render("packing_list.portal_products2", values)


class CustomCustomerPortal(CustomerPortal):
    OPTIONAL_BILLING_FIELDS = ["zipcode", "state_id", "vat", "company_name",
                               "street2"]

    def _prepare_portal_layout_values(self):
        res = super(CustomCustomerPortal, self)._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        if partner.send_email:
            res.update({'send_email': True})
        else:
            res.update({'send_email': False})
        if partner.ref_email:
            res.update({'email': partner.ref_email})
        res.update({'partner': partner})
        return res

    @http.route(['/get_products'], type='http', auth="user", website=True)
    def portal_products(self, page=1, date_begin=None, date_end=None,
                        sortby=None, **kw):
        if request.env.user.partner_id and request.env.user.partner_id.country_id.code == 'GB':
            location_id = request.env['stock.location'].sudo().search([
                ('is_storeship', '=', True)
            ], limit=1)
        else:
            location_id = request.env['stock.location'].sudo().search([
                ('complete_name', '=', 'D-TSR/Stock')
            ], limit=1)
        _logger.info(
            "location_id=--------================:%s-----" % location_id)
        products_list = []
        products = request.env['product.product'].sudo().search([
            ('active', '=', True),
            ('type', '=', 'product'),
            ('show_in_portal', '=', True),
            ('sale_ok', '=', True),
            ('location_id', '=', location_id.id)
        ])
        for product in products:
            product_dict = {}
            qty = request.env['stock.quant']._get_available_quantity(product,
                                                                     location_id)
            _logger.info(
                "product_value=--------================:%s----" % product.name)
            _logger.info("qty=--------================:%s----" % qty)
            _logger.info("qty_available=--------================:%s----" % product.qty_available)

            if qty > 0:
                product_dict.update(
                    {'product_id': product, 'qty_available': product.qty_available,
                     'qty': qty})
                products_list.append(product_dict)

        values = {
            'products': products_list,
            'last_sync': location_id.last_sync
        }
        return request.render("packing_list.portal_products", values)


class AttachControllerWeb(Binary):

    @http.route(['/demo/content',
                 '/demo/content/<string:xmlid>',
                 '/demo/content/<string:xmlid>/<string:filename>',
                 '/demo/content/<int:id>',
                 '/demo/content/<int:id>/<string:filename>',
                 '/demo/content/<int:id>-<string:unique>',
                 '/demo/content/<int:id>-<string:unique>/<string:filename>',
                 '/demo/content/<int:id>-<string:unique>/<path:extra>/<string:filename>',
                 '/demo/content/<string:model>/<int:id>/<string:field>',
                 '/demo/content/<string:model>/<int:id>/<string:field>/<string:filename>'],
                type='http', auth="public")
    def content_common_2(self, xmlid=None, model='ir.attachment', id=None,
                         field='datas',
                         filename=None, filename_field='name', unique=None,
                         mimetype=None,
                         download=None, data=None, token=None,
                         access_token=None, **kw):
        print("content_common_2------------------------")
        # 'http://0.0.0.0:8069/web/content?model=sale.order&field=attach_delivery_list&filename_field=binary_fname&id=688&download=true'
        if model == "ir.attachment":
            attachment_id = request.env['ir.attachment'].sudo().search(
                [('id', '=', id), ('access_token', '=', access_token)])
            if not attachment_id:
                return request.not_found()

        request.uid = SUPERUSER_ID
        return self.content_common(xmlid=xmlid, model=model, id=id,
                                   field=field, filename=filename,
                                   filename_field=filename_field,
                                   download=True,
                                   unique=unique,
                                   access_token=access_token)
