odoo.define("sh_customer_signup.custom_js", function (require) {			

    var publicWidget = require("web.public.widget");

    publicWidget.registry.js_cls_sh_customer_signup_custom_js = publicWidget.Widget.extend({
		selector: "#customer_sign_up_form_section",
		events: {
	        'click .customer_img .fa-link': '_onClickImage',
			'click .customer_type .browse': '_onClickBrowseImage',
			'change .customer_type input[type="file"]': '_onChangeFileInput',
			'click #addBtn_cust': '_onClickAddContactRow',
			'click #addBtn_address': '_onClickAddAddressRow',
			'click #contact_row .remove': '_onClickRemoveContactRow',
			'click #address_row .remove': '_onClickRemoveAddressRow',
			'click #create_customer': '_onClickSubmitForm',
			'click #save_customer': '_onClickSubmitForm',

	    },

		/**
         * @override
         */
        start: function () {
			this.rowIdx = 0;
			$("form[action='/customer_sign_up'] #category_section").multiselect();
			return this._super(...arguments);
		},
		
        _onClickSubmitForm: function (ev) {
            console.log('env======================', $('#loaderIcon'));
//            $('#loaderIcon').css('visibility', 'visible');
//            $('#loaderIcon').show();
		},

	    _onClickImage: function (ev) {
			$(".browse").click(); 
		},
		

	    _onClickBrowseImage: function (ev) {
			var file = $("input[name='customer_image']");
	  		file.trigger("click");
		},
		

	    _onChangeFileInput: function (ev) {
			  var fileName = ev.currentTarget.files[0].name;
			  $(".customer_type .file_img").val(fileName);
				var reader = new window.FileReader();
			        
			        reader.onload = function (ev) {
			            $('.customer_type .preview_img').attr('src', ev.target.result);
			        };
					reader.readAsDataURL(ev.currentTarget.files[0]);

		},
		
		_onClickAddContactRow: function (ev) {
        this.rowIdx = this.rowIdx + 1;
        var customerName = "customer_c_name_" + String(this.rowIdx);
        var customerEmail = "customer_c_email_" + String(this.rowIdx);
        var customerPhone = "customer_c_phone_" + String(this.rowIdx);
        var text =
			'<div class="row c_row '+
			String(this.rowIdx)+
			'">'+ '<div class="col-lg-4 col-sm-12">'+
			  '<div class="form-group">'+
				'<label for="customer_contact_name"><b>Name :</b></label>'+
				'<input type="text" class="form-control" ' +
				' name="'+
				customerName+
				'" required="required"/>'+
				'</div>'+
			    '</div>'+
'<div class="col-lg-4 col-sm-12">'+
			  '<div class="form-group">'+
				'<label for="customer_contact_email"><b>Email :</b></label>'+
				'<input type="email" class="form-control" name="'+
				customerEmail+
				'" required="required"/>'+
				'</div>'+
			    '</div>'+
'<div class="col-lg-4 col-sm-12">'+
			  '<div class="form-group">'+
				'<label for="customer_contact_phone"><b>Phone :</b></label>'+
				'<div class="" style="display: flex;align-items: center;">'+
				'<input type="text" class="form-control" name="'+
				customerPhone+
				'"/>'+
				'<button style="margin-left: 14px;margin-right: 7px;font-size: 22px;padding: 0;color: red;" class="btn remove" type="button"><i class="fa fa-trash"/></button>'+
				'</div>'+
				'</div>'+
			    '</div>'
        $("#contact_row").append(text);
		},

		_onClickAddAddressRow: function (ev) {
		    var html = '';
            this.rowIdx = this.rowIdx + 1;
            var addressType = "address_c_type_" + String(this.rowIdx);
            var addressName = "address_c_name_" + String(this.rowIdx);
            var addressEmail = "address_c_email_" + String(this.rowIdx);
            var addressPhone = "address_c_phone_" + String(this.rowIdx);
            var addressStreet = "address_c_street_" + String(this.rowIdx);
            var addressCity = "address_c_city_" + String(this.rowIdx);
            var addressZip = "address_c_zip_" + String(this.rowIdx);
            html += '<div class="row c_row ' + String(this.rowIdx)+ '">'
            html += '<div class="col-lg-3 col-sm-12">'+
			  '<div class="form-group">'+
                    '<label for="address_contact_type"><b>Contact Type :</b></label>'+
                    '<select class="form-control" aria-label="Default select example"'+
                    ' name="'+
                    addressType+
                    '">'+
                      '<option value="contact">Contact</option>'+
                      '<option value="invoice">Invoice Address</option>'+
                      '<option value="delivery">Delivery Address</option>'+
                      '<option value="other">Other Address</option>'+
                      '<option value="private">Private Address</option>'+
                    '</select>'+
				'</div>'+
			    '</div>'
			html += '<div class="col-lg-3 col-sm-12">'+
                  '<div class="form-group">'+
                    '<label for="address_contact_name"><b>Contact Name :</b></label>'+
                    '<input type="text" class="form-control" ' +
                    ' name="'+
                    addressName+
                    '" required="required"/>'+
                    '</div>'+
                    '</div>'
            html += '<div class="col-lg-3 col-sm-12">'+
                  '<div class="form-group">'+
                    '<label for="address_contact_email"><b>Email :</b></label>'+
                    '<input type="email" class="form-control" name="'+
                    addressEmail+
                    '"/>'+
                    '</div>'+
                    '</div>'
            html += '<div class="col-lg-3 col-sm-12">'+
                  '<div class="form-group">'+
                    '<label for="address_contact_phone"><b>Phone :</b></label>'+
                    '<input type="text" class="form-control" name="'+
                    addressPhone+
                    '"/>'+
                    '</div>'+
                    '</div>'
            html += '<div class="col-lg-3 col-sm-12">'+
                  '<div class="form-group">'+
                    '<label for="customer_contact_street"><b>Street :</b></label>'+
                    '<input type="text" class="form-control" name="'+
                    addressStreet+
                    '"/>'+
                    '</div>'+
                    '</div>'
            html +=  '<div class="col-lg-3 col-sm-12">'+
                  '<div class="form-group">'+
                    '<label for="address_contact_city"><b>City :</b></label>'+
                    '<input type="text" class="form-control" name="'+
                    addressCity+
                    '"/>'+
                    '</div>'+
                    '</div>'
            html += '<div class="col-lg-3 col-sm-12">'+
                  '<div class="form-group">'+
                    '<label for="customer_contact_phone"><b>Zip :</b></label>'+
                    '<div class="" style="display: flex;align-items: center;">'+
                    '<input type="text" class="form-control" name="'+
                    addressZip+
                    '"/>'+
                    '<button style="margin-left: 14px;margin-right: 7px;font-size: 22px;padding: 0;color: red;" class="btn remove" type="button"><i class="fa fa-trash"/></button>'+
                    '</div>'+
                    '</div>'
            html += '</div>'


            $("#address_row").append(html);
		},
		
		_onClickRemoveContactRow: function (ev) {
			ev.currentTarget.closest('.c_row').remove();
		},
		_onClickRemoveAddressRow: function (ev) {
			ev.currentTarget.closest('.c_row').remove();
		},
		
		
	});
});

