# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* sh_customer_signup
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 15.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-03-11 13:37+0000\n"
"PO-Revision-Date: 2022-03-11 13:37+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>City :</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Comment / Note</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Contact Person Details</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Customer Details</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Email :</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Mobile :</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Name :</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Phone :</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Please list products which you are purchasing?</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Product Categories</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Street :</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Website Link :</b>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<b>Zip / Postal Code :</b>"
msgstr ""

#. module: sh_customer_signup
#: model:mail.template,body_html:sh_customer_signup.sh_customer_signup_email_notification
msgid ""
"<div style=\"line-height: 28px;\">\n"
"                Hello There,<br/>\n"
"\t\t\t\tNew Customer <b t-out=\"object.name\"/> is just sign-up,<br/>\n"
"\t\t\t\tYou may take follow-up with him.<br/>\n"
"\t\t\t\t<a class=\"btn btn-primary\" role=\"button\" t-attf-href=\"/web?#id={{object.id}}&amp;model=res.partner\">View Details</a><br/>\n"
"\t\t\t\tThank You...<br/>\n"
"\t\t\t\t\n"
"            </div>\n"
"            \n"
"        "
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<strong>Fail!</strong>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "<strong>Success!</strong>"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Add Contact"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Add Product"
msgstr ""

#. module: sh_customer_signup
#: model:ir.model.fields,field_description:sh_customer_signup.field_website__is_enable_auto_portal_user
msgid "Auto Portal User"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Browse..."
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Company"
msgstr ""

#. module: sh_customer_signup
#: model:ir.model.fields,field_description:sh_customer_signup.field_website__is_enable_company_portal_user
msgid "Company "
msgstr ""

#. module: sh_customer_signup
#: model:ir.model,name:sh_customer_signup.model_res_config_settings
msgid "Config Settings"
msgstr ""

#. module: sh_customer_signup
#: model:ir.model,name:sh_customer_signup.model_res_partner
msgid "Contact"
msgstr ""

#. module: sh_customer_signup
#: model:ir.model.fields,field_description:sh_customer_signup.field_website__is_enable_company_contact_portal_user
msgid "Contacts"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Country"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Country..."
msgstr ""

#. module: sh_customer_signup
#: model:ir.model.fields,field_description:sh_customer_signup.field_res_config_settings__is_enable_auto_portal_user
msgid "Create auto portal user customer?"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Customer Registration Form"
msgstr ""

#. module: sh_customer_signup
#: model:website.menu,name:sh_customer_signup.menu_customer_sign_up_form
msgid "Customer Request"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.sh_customer_signup_res_config_settings
msgid "Customer Sign up Configuration"
msgstr ""

#. module: sh_customer_signup
#: model:ir.model.fields,field_description:sh_customer_signup.field_res_config_settings__is_enable_customer_notification
#: model:ir.model.fields,field_description:sh_customer_signup.field_website__is_enable_customer_notification
msgid "Enable Customer Notification"
msgstr ""

#. module: sh_customer_signup
#: model:ir.model.fields,field_description:sh_customer_signup.field_res_config_settings__is_enable_company_portal_user
msgid "For Company "
msgstr ""

#. module: sh_customer_signup
#: model:ir.model.fields,field_description:sh_customer_signup.field_res_config_settings__is_enable_company_contact_portal_user
msgid "For Contacts"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.sh_customer_signup_res_config_settings
msgid "Get email notification on customer signup."
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Individual"
msgstr ""

#. module: sh_customer_signup
#: model:mail.template,subject:sh_customer_signup.sh_customer_signup_email_notification
msgid "New Customer Signup"
msgstr ""

#. module: sh_customer_signup
#: model:mail.template,name:sh_customer_signup.sh_customer_signup_email_notification
msgid "Notification for responsible person"
msgstr ""

#. module: sh_customer_signup
#: model:ir.model.fields,field_description:sh_customer_signup.field_res_partner__customer_product_categ_ids
#: model:ir.model.fields,field_description:sh_customer_signup.field_res_users__customer_product_categ_ids
msgid "Product Categories"
msgstr ""

#. module: sh_customer_signup
#: model:ir.model.fields,field_description:sh_customer_signup.field_res_partner__customer_products
#: model:ir.model.fields,field_description:sh_customer_signup.field_res_users__customer_products
msgid "Products"
msgstr ""

#. module: sh_customer_signup
#: model:ir.model.fields,field_description:sh_customer_signup.field_res_config_settings__user_ids
#: model:ir.model.fields,field_description:sh_customer_signup.field_website__user_ids
msgid "Responsible Person"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "State / Province"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "State / Province..."
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Street..."
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Street2..."
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Submit"
msgstr ""

#. module: sh_customer_signup
#: model_terms:ir.ui.view,arch_db:sh_customer_signup.customer_sign_up_form_view
#: model_terms:website.page,arch_db:sh_customer_signup.customer_sign_up_form_page
msgid "Upload File"
msgstr ""

#. module: sh_customer_signup
#: model:ir.model,name:sh_customer_signup.model_website
msgid "Website"
msgstr ""
