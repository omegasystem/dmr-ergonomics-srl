# -*- coding: utf-8 -*-
from odoo import models, fields, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    customer_products = fields.Text('Products')
    customer_product_categ_ids = fields.Many2many(
        comodel_name='product.category',
        relation='cust_categ_sh_customer_signup_rel',
        string='Product Categories'
    )
    contact_name = fields.Char('Contact Name')
    company_reg_num = fields.Char('Company Registration Num')
    type_of_business = fields.Selection([
        ('dealer', 'Dealer'),
        ('wholesaler', 'Wholesaler'),
        ('ecommerce', 'Ecommerce'),
        ('retailer', 'Retailer'),
        ('distributor', 'Distributor'),
        ('other', 'Other')
    ], string="Type of Business")
    buying_group = fields.Char('Buying Group')

    