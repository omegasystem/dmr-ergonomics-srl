# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.

from odoo import http, fields
from odoo.http import request
import json
import base64

from odoo import fields, api, SUPERUSER_ID

_region_specific_vat_codes = {
    'xi',
}


class CreateCustomer(http.Controller):

    @http.route(['/thank-you'], type='http', auth="public",
                website=True)
    def thank_you(self, **post):
        return request.render("sh_customer_signup.thank_you_page")

    def vat_check(self, vat, country_id):
        partner_obj = request.env['res.partner']
        vat_country_code, vat_number = vat[:2].lower(), vat[2:].replace(' ',
                                                                        '')
        company = request.env.company
        eu_countries = request.env.ref('base.europe').country_ids
        for partner in self:
            if not partner.vat:
                continue
            is_eu_country = partner.commercial_partner_id.country_id in eu_countries
            if company.vat_check_vies and is_eu_country and partner.is_company:
                check_func = partner_obj.vies_vat_check()
            else:
                check_func = partner_obj.simple_vat_check()
            failed_check = False
            vat_has_legit_country_code = self.env['res.country'].search(
                [('code', '=', vat_country_code.upper())])
            if not vat_has_legit_country_code:
                vat_has_legit_country_code = vat_country_code.lower() in _region_specific_vat_codes
            if vat_has_legit_country_code:
                failed_check = not check_func(vat_country_code, vat_number)
            partner_country_code = partner.commercial_partner_id.country_id.code
            if (
                    not vat_has_legit_country_code or failed_check) and partner_country_code:
                failed_check = not check_func(partner_country_code.lower(),
                                              partner.vat)

            if failed_check:
                country_code = partner_country_code or vat_country_code
                msg = partner._construct_constraint_msg(
                    country_code.lower() if country_code else None)
                print("msg---------------", msg)

    @http.route(['/customer_sign_up_address'], type='http', auth="public",
                website=True)
    def create_customer_sign_up_save(self, **post):
        customer_id = request.env['res.partner'].sudo().browse(
            int(post.get('customer_id')))
        contact_dic = {k: v for k, v in post.items() if
                       k.startswith('address_c_name_')}
        if customer_id and contact_dic:
            for key, value in contact_dic.items():
                customer_dic = {}
                if "address_c_name_" in key:
                    customer_dic["name"] = value
                    numbered_key = key.replace("address_c_name_",
                                               "") or ''
                    email_key = 'address_c_email_' + numbered_key
                    phone_key = 'address_c_phone_' + numbered_key
                    type_key = 'address_c_type_' + numbered_key
                    street_key = 'address_c_street_' + numbered_key
                    city_key = 'address_c_city_' + numbered_key
                    zip_key = 'address_c_zip_' + numbered_key
                    if post.get(email_key, False):
                        customer_dic["email"] = post.get(email_key)
                    if post.get(phone_key, False):
                        customer_dic["phone"] = post.get(phone_key)
                    if post.get(type_key, False):
                        customer_dic["type"] = post.get(type_key)
                    if post.get(street_key, False):
                        customer_dic["street"] = post.get(street_key)
                    if post.get(city_key, False):
                        customer_dic["city"] = post.get(city_key)
                    if post.get(zip_key, False):
                        customer_dic["zip"] = post.get(zip_key)
                    customer_dic["parent_id"] = customer_id.id
                    # fill list:
                    request.env["res.partner"].with_context(
                        no_vat_validation=True).sudo().create(
                        customer_dic)
        # res_values = self.common_method(post)
        return request.render("sh_customer_signup.thank_you_page")

    @http.route(['/customer_sign_up_return'], type='http', auth="public",
                website=True)
    def create_customer_save_return(self, **post):
        countries = request.env["res.country"].sudo().search([])
        country_states = request.env["res.country"].state_ids
        res_values = {}
        if post:
            customer_vat = post.get('customer_vat', False)
            customer_email = post.get('customer_email', False)
            check_customer_vat = request.env['res.partner'].sudo().search([
                ('vat', '=', customer_vat)
            ])
            check_customer_email = request.env['res.partner'].sudo().search([
                ('email', '=', customer_email)
            ])
            if check_customer_vat or check_customer_email:
                quote_msg = {
                    'fail': 'VAT already exist or Email!'
                }
            if check_customer_vat or check_customer_email:
                res_values.update({
                    'page_name': 'customer_sign_up_form_page',
                    'default_url': '/customer_sign_up',
                    'quote_msg': quote_msg,
                    'country_states': country_states,
                    'countries': countries,
                    'customer_name': post.get('customer_name', False),
                    'customer_email': post.get('customer_email', False),
                    'customer_phone': post.get('customer_phone', False),
                    'customer_mobile': post.get('customer_mobile', False),
                    'customer_street': post.get('customer_street', False),
                    'customer_street2': post.get('customer_street2', False),
                    'customer_website': post.get('customer_website', False),
                    'customer_vat': post.get('customer_vat', False),
                    'customer_zip_code': post.get('customer_zip_code', False),
                    'customer_city': post.get('customer_city', False),
                    'contact_name': post.get('contact_name', False),
                    'company_reg_num': post.get('company_reg_num', False),
                    'type_of_business': post.get('type_of_business', False),
                    'buying_group': post.get('buying_group', False),
                })
                return request.render(
                    "sh_customer_signup.customer_sign_up_form_view",
                    res_values)
        values = self.common_method(post)
        print("values-----------", values)
        return request.render("sh_customer_signup.customer_sign_up_form_view2",
                              values)

    @http.route(['/customer_sign_up'], type='http', auth="public",
                website=True)
    def create_customer(self, **post):
        res_values = self.common_method(post)
        return request.render("sh_customer_signup.customer_sign_up_form_view",
                              res_values)

    def common_method(self, post):
        print("post----------------", post)
        quote_msg = {}
        emails = []
        image = 0
        multi_users_value = [0]
        contacts = []
        res_values = {}
        countries = request.env["res.country"].sudo().search([])
        country_states = request.env["res.country"].state_ids
        customer_obj = request.env['res.partner'].sudo()
        if post:
            customer_name = post.get('customer_name', False)
            customer_email = post.get('customer_email', False)
            customer_phone = post.get('customer_phone', False)
            customer_mobile = post.get('customer_mobile', False)
            customer_street = post.get('customer_street', False)
            customer_street2 = post.get('customer_street2', False)
            customer_website = post.get('customer_website', False)
            customer_vat = post.get('customer_vat', False)
            customer_zip_code = post.get('customer_zip_code', False)
            customer_city = post.get('customer_city', False)
            contact_name = post.get('contact_name', False)
            company_reg_num = post.get('company_reg_num', False)
            type_of_business = post.get('type_of_business', False)
            buying_group = post.get('buying_group', False)
            customer_country = post.get('country_id', False)
            customer_state = post.get('state_id', False)
            exit_customer_id = customer_obj.search([
                ('email', '=', customer_email)
            ])
            print("type_of_business-------", type_of_business)
            check_customer_vat = request.env['res.partner'].sudo().search([
                ('vat', '=', customer_vat)
            ])
            print("exit_customer_id-----------------", exit_customer_id)
            if exit_customer_id or check_customer_vat:
                quote_msg = {
                    'fail': 'VAT already exist or Email!'
                }
                res_values.update({
                    'page_name': 'customer_sign_up_form_page',
                    'default_url': '/customer_sign_up',
                    'quote_msg': quote_msg,
                    'country_states': country_states,
                    'countries': countries,
                    'customer_name': post.get('customer_name', False),
                    'customer_email': post.get('customer_email', False),
                    'customer_phone': post.get('customer_phone', False),
                    'customer_mobile': post.get('customer_mobile', False),
                    'customer_street': post.get('customer_street', False),
                    'customer_street2': post.get('customer_street2', False),
                    'customer_website': post.get('customer_website', False),
                    'customer_vat': post.get('customer_vat', False),
                    'customer_zip_code': post.get('customer_zip_code', False),
                    'customer_city': post.get('customer_city', False),
                    'contact_name': post.get('contact_name', False),
                    'company_reg_num': post.get('company_reg_num', False),
                    'type_of_business': post.get('type_of_business', False),
                    'buying_group': post.get('buying_group', False),
                })
                return res_values

            # Country
            customer_country = post.get('country_id', False)
            if customer_country in ['', "", False, 0]:
                customer_country = False
            else:
                customer_country = int(customer_country)

            # States
            customer_state = post.get('state_id', False)
            if customer_state in ['', "", False, 0]:
                customer_state = False
            else:
                customer_state = int(customer_state)

            customer_type = post.get('customer_type', False)
            customer_comment = post.get('customer_comment', False)
            customer_note = post.get('customer_note', False)
            if post.get('customer_image', False):
                img = post.get('customer_image')
                image = base64.b64encode(img.read())
            multi_users_value = request.httprequest.form.getlist(
                'category_section')
            for l in range(0, len(multi_users_value)):
                multi_users_value[l] = int(multi_users_value[l])
            country = 'country_id' in post and post['country_id'] != '' and \
                      request.env['res.country'].browse(
                          int(post['country_id']))
            country = country and country.exists()
            company_id = False
            pricelist_id = False
            if country:
                if country.code == 'GB':
                    company_id = 2
                    pricelist_id = 2
                else:
                    company_id = 1
            customer_dic = {
                'name': customer_name,
                'street': customer_street,
                'street2': customer_street2,
                'phone': customer_phone,
                'mobile': customer_mobile,
                'email': customer_email,
                'website': customer_website,
                'vat': customer_vat,
                'zip': customer_zip_code,
                'city': customer_city,
                'company_id': int(company_id) if int(company_id) else False,
                'country_id': int(customer_country) if int(
                    customer_country) else False,
                'state_id': int(customer_state) if int(
                    customer_state) else False,
                'company_type': customer_type,
                'customer_products': customer_comment,
                'comment': customer_note,
                'image_1920': image,
                'customer_product_categ_ids': [(
                    6, 0, multi_users_value)] or [],
                'customer_rank': 1,
                'supplier_rank': 0,
                'pricelist_id': pricelist_id,
                'contact_name': contact_name,
                'company_reg_num': company_reg_num,
                'type_of_business': type_of_business,
                'buying_group': buying_group,
            }
            customer_id = customer_obj.with_context(
                no_vat_validation=True).create(customer_dic)
            print("customer_id----------------------------", customer_id)
            if customer_id:
                quote_msg = {
                    'success': 'Customer ' + customer_name + ' created successfully.',
                    'customer_id': customer_id.id
                }
                if request.website.is_enable_customer_notification and request.website.sudo().user_ids.sudo():
                    for user in request.website.user_ids.sudo():
                        if user.sudo().partner_id.sudo() and user.sudo().partner_id.sudo().email:
                            emails.append(user.sudo().partner_id.sudo().email)
                email_values = {
                    'email_to': ','.join(emails),
                    'email_from': request.website.company_id.sudo().email,
                }
                url = ''
                base_url = request.env['ir.config_parameter'].sudo(
                ).get_param('web.base.url')
                url = base_url + "/web#id=" + \
                      str(customer_id.id) + \
                      "&&model=res.partner&view_type=form"
                ctx = {
                    "customer_url": url,
                }
                template_id = request.env['ir.model.data'].get_object(
                    'sh_customer_signup',
                    'sh_customer_signup_email_notification')
                _ = request.env['mail.template'].sudo().browse(
                    template_id.id).with_context(ctx).send_mail(
                    customer_id.id, email_values=email_values,
                    notif_layout='mail.mail_notification_light',
                    force_send=True)

            contact_dic = {k: v for k, v in post.items() if
                           k.startswith('address_c_name_')}
            if customer_id and contact_dic:
                for key, value in contact_dic.items():
                    customer_dic = {}
                    if "address_c_name_" in key:
                        customer_dic["name"] = value
                        numbered_key = key.replace("address_c_name_",
                                                   "") or ''
                        email_key = 'address_c_email_' + numbered_key
                        phone_key = 'address_c_phone_' + numbered_key
                        type_key = 'address_c_type_' + numbered_key
                        street_key = 'address_c_street_' + numbered_key
                        city_key = 'address_c_city_' + numbered_key
                        zip_key = 'address_c_zip_' + numbered_key
                        if post.get(email_key, False):
                            customer_dic["email"] = post.get(email_key)
                        if post.get(phone_key, False):
                            customer_dic["phone"] = post.get(phone_key)
                        if post.get(type_key, False):
                            customer_dic["type"] = post.get(type_key)
                        if post.get(street_key, False):
                            customer_dic["street"] = post.get(street_key)
                        if post.get(city_key, False):
                            customer_dic["city"] = post.get(city_key)
                        if post.get(zip_key, False):
                            customer_dic["zip"] = post.get(zip_key)
                        customer_dic["parent_id"] = customer_id.id
                        # fill list:
                        contact_id = request.env["res.partner"].with_context(
                            no_vat_validation=True).sudo().create(
                            customer_dic)
                        if contact_id:
                            contacts.append(contact_id.id)

            try:
                if request.website.is_enable_auto_portal_user:
                    if request.website.is_enable_company_portal_user:
                        user_id = request.env['res.users'].sudo().search(
                            [('partner_id', '=', customer_id.id)], limit=1)
                        if not user_id and customer_id:
                            portal_wizard_obj = request.env['portal.wizard']
                            created_portal_wizard = portal_wizard_obj.sudo().create(
                                {})
                            if created_portal_wizard and customer_id.email and request.env.user:
                                portal_wizard_user_obj = request.env[
                                    'portal.wizard.user']
                                wiz_user_vals = {
                                    'wizard_id': created_portal_wizard.id,
                                    'partner_id': customer_id.id,
                                    'email': customer_id.email,
                                    'in_portal': True,
                                }
                                created_portal_wizard_user = portal_wizard_user_obj.sudo().create(
                                    wiz_user_vals)
                                if created_portal_wizard_user:
                                    created_portal_wizard.sudo().with_user(
                                        SUPERUSER_ID).action_apply()
                    if request.website.is_enable_company_contact_portal_user:
                        if len(contacts) > 0:
                            for contact in contacts:
                                user_id = request.env[
                                    'res.users'].sudo().search(
                                    [('partner_id', '=', contact)], limit=1)
                                partner = request.env[
                                    'res.partner'].sudo().browse(contact)
                                if not user_id and partner:
                                    portal_wizard_obj = request.env[
                                        'portal.wizard']
                                    created_portal_wizard = portal_wizard_obj.sudo().create(
                                        {})
                                    if created_portal_wizard and customer_id.email and request.env.user:
                                        portal_wizard_user_obj = request.env[
                                            'portal.wizard.user']
                                        wiz_user_vals = {
                                            'wizard_id': created_portal_wizard.id,
                                            'partner_id': partner.id,
                                            'email': partner.email,
                                            'in_portal': True,
                                        }
                                        created_portal_wizard_user = portal_wizard_user_obj.sudo().create(
                                            wiz_user_vals)
                                        if created_portal_wizard_user:
                                            created_portal_wizard.sudo().with_user(
                                                SUPERUSER_ID).action_apply()
            except Exception as e:
                quote_msg = {
                    'fail': str(e)
                }
        res_values.update({
            'page_name': 'customer_sign_up_form_page',
            'default_url': '/customer_sign_up',
            'quote_msg': quote_msg,
            'country_states': country_states,
            'countries': countries,
            'customer_name': post.get('customer_name', False),
            'customer_email': post.get('customer_email', False),
            'customer_phone': post.get('customer_phone', False),
            'customer_mobile': post.get('customer_mobile', False),
            'customer_street': post.get('customer_street', False),
            'customer_street2': post.get('customer_street2', False),
            'customer_website': post.get('customer_website', False),
            'customer_vat': post.get('customer_vat', False),
            'customer_zip_code': post.get('customer_zip_code', False),
            'customer_city': post.get('customer_city', False),
            'contact_name': post.get('contact_name', False),
            'company_reg_num': post.get('company_reg_num', False),
            'type_of_business': post.get('type_of_business', False),
            'buying_group': post.get('buying_group', False),
        })
        return res_values

    @http.route(['/customer_sign_up/<model("res.country"):country>'],
                type='json', auth="public", methods=['POST'], website=True)
    def sh_country_infos(self, country, **kw):
        return dict(
            states=[(st.id, st.name, st.code) for st in country.state_ids],
            phone_code=country.phone_code,
            # zip_required=country.zip_required,
            # state_required=country.state_required,
        )
