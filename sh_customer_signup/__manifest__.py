# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.
{
    "name": "Website Customer Signup | Customer Signup Website",
    "author": "Softhealer Technologies",
    "website": "https://www.softhealer.com",
    "support": "support@softhealer.com",
    "license": "OPL-1",
    "category": "Website",
    "summary": "Customer Registration Form Marketplace Multi Step Customer Signup Odoo Customer Portal Website Customer Registration Form Website Portal for Customer Sign up Form Web Customer Form Customer Sign up Customer Odoo",
    "description": """In this module, we have added a customer sign up form on the website. So customer can do registration/sign up from the website. The responsible persons get an email notification on the customer sign up. We provide the option for a customer to the auto-create portal user when signup.""",
    "version": "13.0.1",
    "depends": [
        "website",
        "sale",
    ],
    "application": True,
    "data": [
        "views/assets_frontend.xml",
        "views/customer_sign_up_template.xml",
        "views/res_partner_view_inherit.xml",
        "views/res_config_settings.xml",
        "views/user_portal.xml",
        "data/customer_sign_up_menu.xml",
        "data/mail_template.xml",
    ],
    "images": ["static/description/background.png", ],  
    "auto_install": False,
    "installable": True,
    "price": 109,
    "currency": "EUR",
}
