# -*- coding: utf-8 -*-

import time
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero
from datetime import datetime
from dateutil.relativedelta import relativedelta


class ReportInvoiceExtended(models.AbstractModel):
    _name = 'report.invoice_extends.report_invoice_extended'

    def get_amount(self, amount=1.0, record=False):
        target_currency_id = self.env['res.currency'].search([
            ('name', '=', 'RON')
        ])
        from_currency_id = self.env['res.currency'].search([
            ('name', '=', record.currency_id.name)
        ])
        if record.invoice_date:
            invoice_date = record.invoice_date
        else:
            invoice_date = fields.Date.from_string(fields.Date.today())
        amount = from_currency_id._convert(amount, target_currency_id, self.env.user.company_id, invoice_date)
        thousands_separator = "."
        fractional_separator = ","
        currency = "{:,.2f}".format(amount)
        if thousands_separator == ".":
            main_currency, fractional_currency = currency.split(".")[0], currency.split(".")[1]
            new_main_currency = main_currency.replace(",", ".")
            currency = new_main_currency + fractional_separator + fractional_currency
        return currency

    def get_amount_curs(self, amount=1.0, record=False):
        target_currency_id = self.env['res.currency'].search([
            ('name', '=', 'RON')
        ])
        company_id = self.env['res.company'].browse(1)
        from_currency_id = self.env['res.currency'].search([
            ('name', '=', record.currency_id.name)
        ])
        date_from = fields.Date.from_string(fields.Date.today())
        amount = from_currency_id._convert(amount, target_currency_id, company_id, record.invoice_date, round=False)
        return '%.4f' % amount

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['account.move'].browse(docids)
        return {
            'doc_ids': docs.ids,
            'doc_model': 'account.move',
            'data': data,
            'docs': docs,
            'get_amount': self.get_amount,
            'get_amount_curs': self.get_amount_curs
        }


class ReportInvoiceExtendedUk(models.AbstractModel):
    _name = 'report.invoice_extends.report_invoice_extended_uk'

    def convert_amount(self, amount):
        format_one = "{:.2f}".format(amount)
        format_two = "{:,}".format(float(format_one))
        print("format_two----------", format_two)
        return str(format_two.replace(',', '.'))

    def get_amount(self, amount=1.0, record=False):
        print("amount------------", amount)
        target_currency_id = self.env['res.currency'].search([
            ('name', '=', 'RON')
        ])
        from_currency_id = self.env['res.currency'].search([
            ('name', '=', record.currency_id.name)
        ])
        date_from = fields.Date.from_string(fields.Date.today())
        amount = from_currency_id._convert(amount, target_currency_id, self.env.user.company_id, record.invoice_date)
        format_one = "{:.2f}".format(amount)
        # if amount == 1.0:
        #     return format_one
        print("format_one----------", format_one)
        format_two = "{:,}".format(float(format_one))
        print("format_two----------", format_two)
        return str(format_two.replace(',', '.'))

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['account.move'].browse(docids)
        return {
            'doc_ids': docs.ids,
            'doc_model': 'account.move',
            'data': data,
            'docs': docs,
            'get_amount': self.get_amount,
            'convert_amount': self.convert_amount,
        }


class ReportInvoiceExtendedDmrRo(models.AbstractModel):
    _name = 'report.invoice_extends.report_invoice_extended_dmr_ro'

    # def get_amount_curs(self, amount=1.0, record=False):
    #     target_currency_id = self.env['res.currency'].search([
    #         ('name', '=', 'RON')
    #     ])
    #     from_currency_id = self.env['res.currency'].search([
    #         ('name', '=', record.currency_id.name)
    #     ])
    #     date_from = fields.Date.from_string(fields.Date.today())
    #     amount = from_currency_id._convert(amount, target_currency_id, self.env.user.company_id, record.invoice_date, round=False)
    #     return '%.4f' % amount

    def get_amount_curs(self, amount=1.0, record=False):
        target_currency_id = self.env['res.currency'].search([
            ('name', '=', 'RON')
        ])
        company_id = self.env['res.company'].browse(1)
        from_currency_id = self.env['res.currency'].search([
            ('name', '=', record.currency_id.name)
        ])
        date_from = fields.Date.from_string(fields.Date.today())
        amount = from_currency_id._convert(amount, target_currency_id, company_id, record.invoice_date, round=False)
        return '%.4f' % amount

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['account.move'].browse(docids)
        sale_id = self.env['sale.order'].search([
            ('name', '=', docs.invoice_origin)
        ])
        value = {
            'doc_ids': docs.ids,
            'doc_model': 'account.move',
            'data': data,
            'docs': docs,
            'total_amount': docs.amount_total,
            'get_amount_curs': self.get_amount_curs,
            'get_currnet_user': self.env.user.name,
            'company_currency_symbol': self.env.company.currency_id.symbol
        }
        if sale_id:
            value.update({
                'dmr_delivery_addr':sale_id.street,
                'city': sale_id.city,
                'state':sale_id.state_id.name,
                'country': sale_id.country_id.name,
                'is_cust_add':sale_id.is_address_custom,
                'gross_weight' : sale_id.gross_weight,
                'net_weight' : sale_id.net_weight,
                'term_of_delivery' : sale_id.term_of_delivery,
                'sale_id': sale_id,
                'count_no_pcs_ids': len(sale_id.no_pcs_ids)
            })
        return value
