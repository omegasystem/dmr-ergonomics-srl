from odoo import api, fields, models
from babel.numbers import format_decimal


class AccountMove(models.Model):
    _inherit = "account.move"

    autofactura = fields.Char('Autofactura')

    # dmr_ro_delivery_address = fields.Many2one(
    #     'res.partner',
    #     string='Delivery Address',
    #     domain="[('type', '=', 'delivery')]")


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    ron_price_unit = fields.Char('Price Unit')
    ron_price_subtotal = fields.Char('Price Subtotal')
    ron_price_total = fields.Char('Price Total')

    @api.model
    def _get_price_total_and_subtotal_model(self, price_unit, quantity, discount, currency, product, partner, taxes,
                                            move_type):

        res = {}
        # Compute 'price_subtotal'.
        price_unit_wo_discount = price_unit * (1 - (discount / 100.0))
        subtotal = quantity * price_unit_wo_discount

        # Compute 'price_total'.
        if taxes:
            force_sign = -1 if move_type in ('out_invoice', 'in_refund', 'out_receipt') else 1
            taxes_res = taxes._origin.with_context(force_sign=force_sign).compute_all(price_unit_wo_discount,
                                                                                      quantity=quantity,
                                                                                      currency=currency,
                                                                                      product=product, partner=partner,
                                                                                      is_refund=move_type in (
                                                                                      'out_refund', 'in_refund'))
            # "{:,}".format(float(taxes_res['total_excluded']))

            res['price_subtotal'] = taxes_res['total_excluded']
            res['price_total'] = taxes_res['total_included']

        else:
            res['price_total'] = res['price_subtotal'] = subtotal
        # In case of multi currency, round before it's use for computing debit credit
        if currency:
            res = {k: currency.round(v) for k, v in res.items()}
        return res

