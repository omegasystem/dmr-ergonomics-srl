from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    extra_note = fields.Text('Extra Notes')
    awb_no = fields.Char('AWB')
    business_name = fields.Char('Business Name')
    street = fields.Char('Street')
    street2 = fields.Char('Street2')
    zip = fields.Char('Zip')
    city = fields.Char('City')
    state_id = fields.Many2one("res.country.state", string='State')
    country_id = fields.Many2one('res.country', string='Country')
    tel = fields.Char('Tel')
    email = fields.Char('Email')
    no_piece = fields.Char('No of Piece')
    jd_line_1 = fields.Char('JD 1')
    jd_line_2 = fields.Char('JD 2')
    jd_line_3 = fields.Char('JD 3')
    gross_weight = fields.Char('Gross Weight')
    net_weight = fields.Char('Net Weight')
    term_of_delivery = fields.Char('Terms of Delivery')
    dmr_ro_delivery_address = fields.Many2one(
        'res.partner',
        string='Delivery Address',
        domain="[('type', '=', 'delivery')]")
    is_address_custom = fields.Boolean("Is Custom Address?")
    awb_in_ids = fields.One2many('awb.lines', 'sale_id', string='AWS')
    no_pcs_ids = fields.One2many('no.pieces', 'sale_id', string='No of Pieces')

    @api.model
    def create(self, vals):
        res = super(SaleOrder, self).create(vals)
        # res.action_confirm()
        return res


class ProductTemplate(models.Model):
    _inherit = "product.template"

    origin_country_id = fields.Many2one('res.country', string='Country')


class AwbLines(models.Model):
    _name = 'awb.lines'

    name = fields.Char("AWB No")
    sale_id = fields.Many2one('sale.order', string='Sale')


class NoPieces(models.Model):
    _name = 'no.pieces'

    name = fields.Char("No Pieces")
    sale_id = fields.Many2one('sale.order', string='Sale')
