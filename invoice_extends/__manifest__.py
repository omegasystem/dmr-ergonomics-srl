{
    'name': 'Invoice Extends',
    'version': '1.0',
    'author': '',
    'website': '',
    'license': 'AGPL-3',
    'category': 'invoice',
    'summary': 'Invoice Extends',
    'depends': [
        'base',
        'account',
        'sale',
        'currency_rate_update',
        'delivery',
        'stock'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/sale_view.xml',
        'views/product_views.xml',
        'views/account_move_views.xml',
        # 'views/account_invoice_view.xml',
        'report/invoice_template_uk.xml',
        'report/invoice_template_dmr_ro.xml',
        'report/invoice_template.xml',
        'report/account_invoice.xml',
        'report/invoice_reports.xml',
        'data/mail_template.xml'
    ],
    'installable': True,
}
