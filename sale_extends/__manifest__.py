{
    'name': 'Sale Extends',
    'version': '1.0',
    'author': '',
    'website': '',
    'license': 'AGPL-3',
    'category': 'sales',
    'summary': 'Sale Extends',
    'depends': [
        'base',
        'sale',
        'hr'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/employee_view.xml',
    ],
    'installable': True,
}
