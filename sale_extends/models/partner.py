from odoo import api, fields, models


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    document_ids = fields.One2many('employee.documents', 'employee_id', string="Document")


class EmployeeDocuments(models.Model):
    _name = "employee.documents"

    employee_id = fields.Many2one('hr.employee', string="employee")
    name = fields.Many2one('document.type', string="Document Type")
    attachment = fields.Binary('Attachment')


class DocumentType(models.Model):
    _name = "document.type"

    name = fields.Char('Name')