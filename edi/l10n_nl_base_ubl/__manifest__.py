# Copyright 2020 Onestein (<https://www.onestein.eu>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Base UBL - Netherlands",
    "version": "13.0.1.0.0",
    "development_status": "Beta",
    "category": "Localization/Netherlands",
    "license": "AGPL-3",
    "summary": "Base module for Netherlands-specific UBL dialect (NLCIUS)",
    "author": "Onestein, Odoo Community Association (OCA)",
    "maintainers": ["astirpe", "thomaspaulb"],
    "website": "https://github.com/OCA/l10n-netherlands",
    "depends": ["base_ubl"],
    "installable": True,
}
