{
    "name": "Custom xml generate",
    "version": "17.0.1.0.1",
    "category": "Accounting & Finance",
    "summary": "Custom xml generates",
    "author": "Custom xml generates",
    "website": "",
    "license": "AGPL-3",
    "depends": ["account"],
    "data": [
        "views/account_move_views.xml",
        "views/res_partner.xml",
    ],
    "installable": True,
}
