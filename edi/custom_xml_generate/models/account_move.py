from odoo import api, fields, models, _
import xml.etree.cElementTree as ET
from lxml import etree
import logging
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_round, get_lang
import base64

logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    _inherit = "account.move"

    payment_mode_id = fields.Many2one(
        comodel_name="account.payment.mode",
        readonly=False,
    )

    # def action_invoice_sent(self):
    #     self.ensure_one()
    #     template = self.env.ref('account.email_template_edi_invoice', raise_if_not_found=False)
    #     lang = get_lang(self.env)
    #     if template and template.lang:
    #         lang = template._render_template(template.lang, 'account.move', self.id)
    #     else:
    #         lang = lang.code
    #     compose_form = self.env.ref('account.account_invoice_send_wizard_form', raise_if_not_found=False)
    #     file_name = "Invoice_%s_ubl.xml" % self.name
    #     xml_file = self.env['ir.attachment'].search([('res_id', '=', self.id), ('name', '=', file_name)])
    #
    #     ctx = dict(
    #         default_model='account.move',
    #         default_res_id=self.id,
    #         default_res_model='account.move',
    #         default_use_template=bool(template),
    #         default_template_id=template and template.id or False,
    #         default_composition_mode='comment',
    #         mark_invoice_as_sent=True,
    #         custom_layout="mail.mail_notification_paynow",
    #         model_description=self.with_context(lang=lang).type_name,
    #         force_email=True,
    #         default_attachment_ids = [xml_file.id],
    #     )
    #     return {
    #         'name': _('Send Invoice'),
    #         'type': 'ir.actions.act_window',
    #         'view_type': 'form',
    #         'view_mode': 'form',
    #         'res_model': 'account.invoice.send',
    #         'views': [(compose_form.id, 'form')],
    #         'view_id': compose_form.id,
    #         'target': 'new',
    #         'context': ctx,
    #     }

    def get_ubl_filename(self, version="2.1"):
        """This method is designed to be inherited"""
        return "Invoice_%s_ubl.xml" % self.name

    @api.model
    def _ubl_get_nsmap_namespace(self, doc_name, version="2.1"):
        nsmap = {
            None: "urn:oasis:names:specification:ubl:schema:xsd:" + doc_name,
            "cac": "urn:oasis:names:specification:ubl:"
                   "schema:xsd:CommonAggregateComponents-2",
            "cbc": "urn:oasis:names:specification:ubl:schema:xsd:"
                   "CommonBasicComponents-2",
        }
        ns = {
            "cac": "{urn:oasis:names:specification:ubl:schema:xsd:"
                   "CommonAggregateComponents-2}",
            "cbc": "{urn:oasis:names:specification:ubl:schema:xsd:"
                   "CommonBasicComponents-2}",
        }
        return nsmap, ns

    @api.model
    def _ubl_add_country(self, country, parent_node, ns, version="2.1"):
        country_root = etree.SubElement(parent_node, ns["cac"] + "Country")
        country_code = etree.SubElement(country_root, ns["cbc"] + "IdentificationCode")
        country_code.text = country.code
        country_name = etree.SubElement(country_root, ns["cbc"] + "Name")
        country_name.text = country.name

    @api.model
    def _ubl_add_address(self, partner, node_name, parent_node, ns, version="2.1"):
        address = etree.SubElement(parent_node, ns["cac"] + node_name)
        if partner.street:
            streetname = etree.SubElement(address, ns["cbc"] + "StreetName")
            streetname.text = partner.street
        if partner.street2:
            addstreetname = etree.SubElement(
                address, ns["cbc"] + "AdditionalStreetName"
            )
            addstreetname.text = partner.street2
        if hasattr(partner, "street3") and partner.street3:
            blockname = etree.SubElement(address, ns["cbc"] + "BlockName")
            blockname.text = partner.street3
        if partner.city:
            city = etree.SubElement(address, ns["cbc"] + "CityName")
            city.text = partner.city
        if partner.zip:
            zip_code = etree.SubElement(address, ns["cbc"] + "PostalZone")
            zip_code.text = partner.zip
        if partner.state_id:
            state = etree.SubElement(address, ns["cbc"] + "CountrySubentity")
            state.text = partner.state_id.name
            state_code = etree.SubElement(address, ns["cbc"] + "CountrySubentityCode")
            state_code.text = partner.state_id.code
        if partner.country_id:
            self._ubl_add_country(partner.country_id, address, ns, version=version)
        else:
            logger.warning("UBL: missing country on partner %s", partner.name)

    @api.model
    def _ubl_get_contact_id(self, partner):
        return False

    @api.model
    def _ubl_add_contact(
            self, partner, parent_node, ns, node_name="Contact", version="2.1"
    ):
        contact = etree.SubElement(parent_node, ns["cac"] + node_name)
        contact_id_text = self._ubl_get_contact_id(partner)
        if contact_id_text:
            contact_id = etree.SubElement(contact, ns["cbc"] + "ID")
            contact_id.text = contact_id_text
        if partner.parent_id:
            contact_name = etree.SubElement(contact, ns["cbc"] + "Name")
            contact_name.text = partner.name or partner.parent_id.name
        phone = partner.phone or partner.commercial_partner_id.phone
        if phone:
            telephone = etree.SubElement(contact, ns["cbc"] + "Telephone")
            telephone.text = phone
        email = partner.email or partner.commercial_partner_id.email
        if email:
            electronicmail = etree.SubElement(contact, ns["cbc"] + "ElectronicMail")
            electronicmail.text = email

    @api.model
    def _ubl_add_language(self, lang_code, parent_node, ns, version="2.1"):
        langs = self.env["res.lang"].search([("code", "=", lang_code)])
        if not langs:
            return
        lang = langs[0]
        lang_root = etree.SubElement(parent_node, ns["cac"] + "Language")
        lang_name = etree.SubElement(lang_root, ns["cbc"] + "Name")
        lang_name.text = lang.name
        lang_code = etree.SubElement(lang_root, ns["cbc"] + "LocaleCode")
        lang_code.text = lang.code

    @api.model
    def _ubl_get_party_identification(self, commercial_partner):
        """This method is designed to be inherited in localisation modules
        Should return a dict with key=SchemeName, value=Identifier"""
        return {"ID": commercial_partner.vat}

    @api.model
    def _ubl_add_party_identification(
            self, commercial_partner, parent_node, ns, version="2.1"
    ):
        id_dict = self._ubl_get_party_identification(commercial_partner)
        if id_dict:
            party_identification = etree.SubElement(
                parent_node, ns["cac"] + "PartyIdentification"
            )
            for scheme_name, party_id_text in id_dict.items():
                party_identification_id = etree.SubElement(
                    party_identification, ns["cbc"] + "ID", schemeName=scheme_name
                )
                party_identification_id.text = party_id_text
        return

    @api.model
    def _ubl_get_tax_scheme_dict_from_partner(self, commercial_partner):
        tax_scheme_dict = {"id": "VAT", "name": False, "type_code": False}
        return tax_scheme_dict

    @api.model
    def _ubl_add_party_tax_scheme(
            self, commercial_partner, parent_node, ns, version="2.1"
    ):
        if commercial_partner.vat:
            party_tax_scheme = etree.SubElement(
                parent_node, ns["cac"] + "PartyTaxScheme"
            )
            registration_name = etree.SubElement(
                party_tax_scheme, ns["cbc"] + "RegistrationName"
            )
            registration_name.text = commercial_partner.name
            company_id = etree.SubElement(party_tax_scheme, ns["cbc"] + "CompanyID")
            company_id.text = commercial_partner.vat
            tax_scheme_dict = self._ubl_get_tax_scheme_dict_from_partner(
                commercial_partner
            )
            self._ubl_add_tax_scheme(
                tax_scheme_dict, party_tax_scheme, ns, version=version
            )

    @api.model
    def _ubl_add_party_legal_entity(
            self, commercial_partner, parent_node, ns, version="2.1"
    ):
        party_legal_entity = etree.SubElement(
            parent_node, ns["cac"] + "PartyLegalEntity"
        )
        registration_name = etree.SubElement(
            party_legal_entity, ns["cbc"] + "RegistrationName"
        )
        registration_name.text = commercial_partner.name
        self._ubl_add_address(
            commercial_partner,
            "RegistrationAddress",
            party_legal_entity,
            ns,
            version=version,
        )

    @api.model
    def _ubl_add_party(
            self, partner, company, node_name, parent_node, ns, version="2.1"
    ):
        commercial_partner = partner
        party = etree.SubElement(parent_node, ns["cac"] + node_name)
        if commercial_partner.website:
            website = etree.SubElement(party, ns["cbc"] + "WebsiteURI")
            website.text = commercial_partner.website
        self._ubl_add_party_identification(
            commercial_partner, party, ns, version=version
        )
        party_name = etree.SubElement(party, ns["cac"] + "PartyName")
        name = etree.SubElement(party_name, ns["cbc"] + "Name")
        name.text = commercial_partner.name
        if partner.lang:
            self._ubl_add_language(partner.lang, party, ns, version=version)
        self._ubl_add_address(partner, "PostalAddress", party, ns, version=version)
        self._ubl_add_party_tax_scheme(commercial_partner, party, ns, version=version)
        if commercial_partner.is_company or company:
            self._ubl_add_party_legal_entity(
                commercial_partner, party, ns, version="2.1"
            )
        self._ubl_add_contact(partner, party, ns, version=version)
        party = parent_node.find(ns["cac"] + node_name)

        # PEPPOL-EN16931-R020: EndpointID
        endpoint_dict = partner._get_peppol_endpoint_id()
        endpoint_id = endpoint_dict.get("endpoint_id")
        scheme_id = endpoint_dict.get("scheme_id")
        if scheme_id and endpoint_id:
            endpoint_id_element = etree.SubElement(
                parent_node, ns["cbc"] + "EndpointID", schemeID=scheme_id
            )
            endpoint_id_element.text = endpoint_id
            party_name = party.find(ns["cac"] + "PartyName")
            party_name.addprevious(endpoint_id_element)

        # UBL-CR-143: A UBL invoice should not include the
        #             AccountingSupplierParty Party WebsiteURI
        # UBL-CR-206: A UBL invoice should not include the
        #             AccountingCustomerParty Party WebsiteURI
        website_uri = party.find(ns["cbc"] + "WebsiteURI")
        if website_uri is not None:
            party.remove(website_uri)

        # UBL-CR-166: A UBL invoice should not include the AccountingSupplierParty
        #             Party PostalAddress Country Name
        # UBL-CR-229: A UBL invoice should not include the AccountingCustomerParty
        #             Party PostalAddress Country Name
        postal_address = party.find(ns["cac"] + "PostalAddress")
        if postal_address is not None:
            country = postal_address.find(ns["cac"] + "Country")
            if country is not None:
                country_name = country.find(ns["cbc"] + "Name")
                if country_name is not None:
                    country.remove(country_name)

        # UBL-CR-209: A UBL invoice should not include the AccountingCustomerParty
        #             Party Language
        # UBL-CR-146: A UBL invoice should not include the AccountingSupplierParty
        #             Party Language
        if party is not None:
            language = party.find(ns["cac"] + "Language")
            if language is not None:
                party.remove(language)

    def _ubl_get_customer_assigned_id(self, partner):
        return partner.commercial_partner_id.ref

    @api.model
    def _ubl_add_customer_party(
            self, partner, company, node_name, parent_node, ns, version="2.1"
    ):
        """Please read the docstring of the method _ubl_add_supplier_party"""
        if company:
            if partner:
                assert (
                        partner.commercial_partner_id == company.partner_id
                ), "partner is wrong"
            else:
                partner = company.partner_id
        customer_party_root = etree.SubElement(parent_node, ns["cac"] + node_name)
        partner_ref = self._ubl_get_customer_assigned_id(partner)
        if partner_ref:
            customer_ref = etree.SubElement(
                customer_party_root, ns["cbc"] + "SupplierAssignedAccountID"
            )
            customer_ref.text = partner_ref
        self._ubl_add_party(
            partner, company, "Party", customer_party_root, ns, version=version
        )
        # TODO: rewrite support for AccountingContact + add DeliveryContact
        # Additional optional args
        if partner and not company and partner.parent_id:
            self._ubl_add_contact(
                partner,
                customer_party_root,
                ns,
                node_name="AccountingContact",
                version=version,
            )
        return customer_party_root

    @api.model
    def _ubl_add_supplier_party(
            self, partner, company, node_name, parent_node, ns, version="2.1"
    ):
        if company:
            if partner:
                assert (
                        partner.commercial_partner_id == company.partner_id
                ), "partner is wrong"
            else:
                partner = company.partner_id
        supplier_party_root = etree.SubElement(parent_node, ns["cac"] + node_name)
        partner_ref = self._ubl_get_customer_assigned_id(partner)
        if partner_ref:
            supplier_ref = etree.SubElement(
                supplier_party_root, ns["cbc"] + "CustomerAssignedAccountID"
            )
            supplier_ref.text = partner_ref
        self._ubl_add_party(
            partner, company, "Party", supplier_party_root, ns, version=version
        )
        return supplier_party_root

    @api.model
    def _ubl_add_delivery(self, delivery_partner, parent_node, ns, version="2.1"):
        delivery = etree.SubElement(parent_node, ns["cac"] + "Delivery")
        delivery_location = etree.SubElement(delivery, ns["cac"] + "DeliveryLocation")
        self._ubl_add_address(
            delivery_partner, "Address", delivery_location, ns, version=version
        )
        self._ubl_add_party(
            delivery_partner, False, "DeliveryParty", delivery, ns, version=version
        )

    @api.model
    def _ubl_add_payment_terms(self, payment_term, parent_node, ns, version="2.1"):
        pay_term_root = etree.SubElement(parent_node, ns["cac"] + "PaymentTerms")
        pay_term_note = etree.SubElement(pay_term_root, ns["cbc"] + "Note")
        pay_term_note.text = payment_term.name

    @api.model
    def _ubl_add_line_item(
            self,
            line_number,
            name,
            product,
            type_,
            quantity,
            uom,
            parent_node,
            ns,
            seller=False,
            currency=False,
            price_subtotal=False,
            qty_precision=3,
            price_precision=2,
            taxes=None,
            version="2.1",
    ):
        line_item = etree.SubElement(parent_node, ns["cac"] + "LineItem")
        line_item_id = etree.SubElement(line_item, ns["cbc"] + "ID")
        line_item_id.text = str(line_number)
        if not uom.unece_code:
            raise UserError(_("Missing UNECE code on unit of measure '%s'") % uom.name)
        quantity_node = etree.SubElement(
            line_item, ns["cbc"] + "Quantity", unitCode=uom.unece_code
        )
        quantity_node.text = str(quantity)
        if currency and price_subtotal:
            line_amount = etree.SubElement(
                line_item, ns["cbc"] + "LineExtensionAmount", currencyID=currency.name
            )
            line_amount.text = str(price_subtotal)
            price_unit = 0.0
            # Use price_subtotal/qty to compute price_unit to be sure
            # to get a *tax_excluded* price unit
            if not float_is_zero(quantity, precision_digits=qty_precision):
                price_unit = float_round(
                    price_subtotal / float(quantity), precision_digits=price_precision
                )
            price = etree.SubElement(line_item, ns["cac"] + "Price")
            price_amount = etree.SubElement(
                price, ns["cbc"] + "PriceAmount", currencyID=currency.name
            )
            price_amount.text = str(price_unit)
            base_qty = etree.SubElement(
                price, ns["cbc"] + "BaseQuantity", unitCode=uom.unece_code
            )
            base_qty.text = "1"  # What else could it be ?
        self._ubl_add_item(
            name,
            product,
            line_item,
            ns,
            type_=type_,
            seller=seller,
            version=version,
            taxes=taxes,
        )

    def _ubl_get_seller_code_from_product(self, product):
        """Inherit and overwrite if another custom product code is required"""
        return product.default_code

    def _ubl_get_customer_product_code(self, product, customer):
        """Inherit and overwrite to return the customer product sku either from
        product, invoice_line or customer (product.customer_sku,
        invoice_line.customer_sku, customer.product_sku)"""
        return ""

    @api.model
    def _ubl_add_item(
            self,
            name,
            product,
            parent_node,
            ns,
            type_="purchase",
            seller=False,
            customer=False,
            taxes=None,
            version="2.1",
    ):
        """Beware that product may be False (in particular on invoices)"""
        assert type_ in ("sale", "purchase"), "Wrong type param"
        assert name, "name is a required arg"
        item = etree.SubElement(parent_node, ns["cac"] + "Item")
        product_name = False
        seller_code = False
        if product:
            if type_ == "purchase":
                if seller:
                    sellers = product._select_seller(
                        partner_id=seller, quantity=0.0, date=None, uom_id=False
                    )
                    if sellers:
                        product_name = sellers[0].product_name
                        seller_code = sellers[0].product_code
            if not seller_code:
                seller_code = self._ubl_get_seller_code_from_product(product)
            if not product_name:
                variant = ", ".join(product.attribute_line_ids.mapped("value_ids.name"))
                product_name = (
                        variant and "{} ({})".format(product.name, variant) or product.name
                )
        description = etree.SubElement(item, ns["cbc"] + "Description")
        description.text = name
        name_node = etree.SubElement(item, ns["cbc"] + "Name")
        name_node.text = product_name or name.split("\n")[0]

        customer_code = self._ubl_get_customer_product_code(product, customer)
        if customer_code:
            buyer_identification = etree.SubElement(
                item, ns["cac"] + "BuyersItemIdentification"
            )
            buyer_identification_id = etree.SubElement(
                buyer_identification, ns["cbc"] + "ID"
            )
            buyer_identification_id.text = customer_code
        if seller_code:
            seller_identification = etree.SubElement(
                item, ns["cac"] + "SellersItemIdentification"
            )
            seller_identification_id = etree.SubElement(
                seller_identification, ns["cbc"] + "ID"
            )
            seller_identification_id.text = seller_code
        if product:
            if product.barcode:
                std_identification = etree.SubElement(
                    item, ns["cac"] + "StandardItemIdentification"
                )
                std_identification_id = etree.SubElement(
                    std_identification,
                    ns["cbc"] + "ID",
                    schemeAgencyID="6",
                    schemeID="GTIN",
                )
                std_identification_id.text = product.barcode
            # I'm not 100% sure, but it seems that ClassifiedTaxCategory
            # contains the taxes of the product without taking into
            # account the fiscal position
            if type_ == "sale":
                taxes = product.taxes_id
            else:
                taxes = product.supplier_taxes_id
            if taxes:
                for tax in taxes:
                    self._ubl_add_tax_category(
                        tax,
                        item,
                        ns,
                        node_name="ClassifiedTaxCategory",
                        version=version,
                    )
            for ptav in product.product_template_attribute_value_ids._only_active():
                item_property = etree.SubElement(
                    item, ns["cac"] + "AdditionalItemProperty"
                )
                property_name = etree.SubElement(item_property, ns["cbc"] + "Name")
                property_name.text = ptav.attribute_id.name
                property_value = etree.SubElement(item_property, ns["cbc"] + "Value")
                property_value.text = ptav.name

    @api.model
    def _ubl_add_tax_subtotal(
            self,
            taxable_amount,
            tax_amount,
            tax,
            currency_code,
            parent_node,
            ns,
            version="2.1",
    ):
        prec = self.env["decimal.precision"].precision_get("Account")
        tax_subtotal = etree.SubElement(parent_node, ns["cac"] + "TaxSubtotal")
        if not float_is_zero(taxable_amount, precision_digits=prec):
            taxable_amount_node = etree.SubElement(
                tax_subtotal, ns["cbc"] + "TaxableAmount", currencyID=currency_code
            )
            taxable_amount_node.text = "%0.*f" % (prec, taxable_amount)
        tax_amount_node = etree.SubElement(
            tax_subtotal, ns["cbc"] + "TaxAmount", currencyID=currency_code
        )
        tax_amount_node.text = "%0.*f" % (prec, tax_amount)
        if tax.amount_type == "percent" and not float_is_zero(
                tax.amount, precision_digits=prec + 3
        ):
            percent = etree.SubElement(tax_subtotal, ns["cbc"] + "Percent")
            percent.text = str(float_round(tax.amount, precision_digits=2))
        self._ubl_add_tax_category(tax, tax_subtotal, ns, version=version)

    @api.model
    def _ubl_add_tax_category(
            self, tax, parent_node, ns, node_name="TaxCategory", version="2.1"
    ):
        tax_category = etree.SubElement(parent_node, ns["cac"] + node_name)
        if not tax.unece_categ_id:
            raise UserError(_("Missing UNECE Tax Category on tax '%s'" % tax.name))
        tax_category_id = etree.SubElement(
            tax_category, ns["cbc"] + "ID", schemeID="UN/ECE 5305", schemeAgencyID="6"
        )
        tax_category_id.text = tax.unece_categ_code
        tax_name = etree.SubElement(tax_category, ns["cbc"] + "Name")
        tax_name.text = tax.name
        if tax.amount_type == "percent":
            tax_percent = etree.SubElement(tax_category, ns["cbc"] + "Percent")
            tax_percent.text = str(tax.amount)
        tax_scheme_dict = self._ubl_get_tax_scheme_dict_from_tax(tax)
        self._ubl_add_tax_scheme(tax_scheme_dict, tax_category, ns, version=version)

    @api.model
    def _ubl_get_tax_scheme_dict_from_tax(self, tax):
        if not tax.unece_type_id:
            raise UserError(_("Missing UNECE Tax Type on tax '%s'" % tax.name))
        tax_scheme_dict = {"id": tax.unece_type_code, "name": False, "type_code": False}
        return tax_scheme_dict

    @api.model
    def _ubl_add_tax_scheme(self, tax_scheme_dict, parent_node, ns, version="2.1"):
        tax_scheme = etree.SubElement(parent_node, ns["cac"] + "TaxScheme")
        if tax_scheme_dict.get("id"):
            tax_scheme_id = etree.SubElement(
                tax_scheme, ns["cbc"] + "ID", schemeID="UN/ECE 5153", schemeAgencyID="6"
            )
            tax_scheme_id.text = tax_scheme_dict["id"]
        if tax_scheme_dict.get("name"):
            tax_scheme_name = etree.SubElement(tax_scheme, ns["cbc"] + "Name")
            tax_scheme_name.text = tax_scheme_dict["name"]
        if tax_scheme_dict.get("type_code"):
            tax_scheme_type_code = etree.SubElement(
                tax_scheme, ns["cbc"] + "TaxTypeCode"
            )
            tax_scheme_type_code.text = tax_scheme_dict["type_code"]

    def action_generate_xml(self):
        version = "2.1"
        xml_root = self.with_context(lang=self.partner_id.lang).generate_invoice_ubl_xml_etree(
            version=version
        )
        xml_string = etree.tostring(
            xml_root, pretty_print=True, encoding="UTF-8", xml_declaration=True
        )
        filename = self.get_ubl_filename(version=version)
        attach = (
            self.env["ir.attachment"]
            .with_context({})
            .create(
                {
                    "name": filename,
                    "res_id": self.id,
                    "res_model": self._name,
                    "datas": base64.b64encode(xml_string),
                    # If default_type = 'out_invoice' in context, 'type'
                    # would take 'out_invoice' value by default !
                    "type": "binary",
                }
            )
        )
        action = self.env["ir.attachment"].action_get()
        action.update({"res_id": attach.id, "views": False, "view_mode": "form,tree"})
        return action

    def _ubl_add_header(self, parent_node, ns, version="2.1"):
        self.ensure_one()
        ubl_version = etree.SubElement(parent_node, ns["cbc"] + "UBLVersionID")
        ubl_version.text = version
        doc_id = etree.SubElement(parent_node, ns["cbc"] + "ID")
        doc_id.text = self.name
        issue_date = etree.SubElement(parent_node, ns["cbc"] + "IssueDate")
        issue_date.text = self.invoice_date.strftime("%Y-%m-%d")
        if self.invoice_date_due and version >= "2.1":
            due_date = etree.SubElement(parent_node, ns["cbc"] + "DueDate")
            due_date.text = fields.Date.to_string(self.invoice_date_due)
        type_code = etree.SubElement(parent_node, ns["cbc"] + "InvoiceTypeCode")
        type_code.text = self._ubl_get_invoice_type_code()
        if self.narration:
            note = etree.SubElement(parent_node, ns["cbc"] + "Note")
            note.text = self.narration
        doc_currency = etree.SubElement(parent_node, ns["cbc"] + "DocumentCurrencyCode")
        doc_currency.text = self.currency_id.name
        customization_id = etree.Element(ns["cbc"] + "CustomizationID")
        customization_id.text = (
            "urn:cen.eu:en16931:2017#compliant#urn:fdc:peppol.eu:2017:poacc:billing:3.0"
        )

        # PEPPOL-EN16931-R001, PEPPOL-EN16931-R007: Business process
        profile_id = etree.Element(ns["cbc"] + "ProfileID")
        profile_id.text = "urn:fdc:peppol.eu:2017:poacc:billing:01:1.0"

        # Add items right after UBLVersionID
        ubl_version_id = parent_node.find(ns["cbc"] + "UBLVersionID")
        ubl_version_id.addnext(profile_id)
        ubl_version_id.addnext(customization_id)

    def _ubl_get_invoice_type_code(self):
        return "380"

    def _ubl_get_order_reference(self):
        """This method is designed to be inherited"""
        return self.invoice_origin

    def _ubl_add_order_reference(self, parent_node, ns, version="2.1"):
        self.ensure_one()
        sale_order_ref = self._ubl_get_order_reference()
        if sale_order_ref:
            order_ref = etree.SubElement(parent_node, ns["cac"] + "OrderReference")
            order_ref_id = etree.SubElement(order_ref, ns["cbc"] + "ID")
            order_ref_id.text = sale_order_ref

    def _ubl_get_buyer_reference(self):
        return self.ref

    def _ubl_add_buyer_reference(self, parent_node, ns, version="2.1"):
        self.ensure_one()
        buyer_ref = self._ubl_get_buyer_reference()
        if buyer_ref:
            buyer_order_ref = etree.SubElement(
                parent_node, ns["cbc"] + "BuyerReference"
            )
            buyer_order_ref.text = buyer_ref

    def _ubl_get_contract_document_reference_dict(self):
        """Result: dict with key = Doc Type Code, value = ID"""
        self.ensure_one()
        return {}

    def _ubl_add_contract_document_reference(self, parent_node, ns, version="2.1"):
        self.ensure_one()
        cdr_dict = self._ubl_get_contract_document_reference_dict()
        for doc_type_code, doc_id in cdr_dict.items():
            cdr = etree.SubElement(parent_node, ns["cac"] + "ContractDocumentReference")
            cdr_id = etree.SubElement(cdr, ns["cbc"] + "ID")
            cdr_id.text = doc_id
            cdr_type_code = etree.SubElement(cdr, ns["cbc"] + "DocumentTypeCode")
            cdr_type_code.text = doc_type_code

    def _ubl_add_attachments(self, parent_node, ns, version="2.1"):
        self.ensure_one()
        if self.company_id.embed_pdf_in_ubl_xml_invoice and not self.env.context.get(
            "no_embedded_pdf"
        ):
            filename = "Invoice-" + self.name + ".pdf"
            docu_reference = etree.SubElement(
                parent_node, ns["cac"] + "AdditionalDocumentReference"
            )
            docu_reference_id = etree.SubElement(docu_reference, ns["cbc"] + "ID")
            docu_reference_id.text = filename
            attach_node = etree.SubElement(docu_reference, ns["cac"] + "Attachment")
            binary_node = etree.SubElement(
                attach_node,
                ns["cbc"] + "EmbeddedDocumentBinaryObject",
                mimeCode="application/pdf",
                filename=filename,
            )
            ctx = dict()
            ctx["no_embedded_ubl_xml"] = True
            ctx["force_report_rendering"] = True
            pdf_inv = (
                self.with_context(ctx)
                .env.ref("account.account_invoices")
                .render_qweb_pdf(self.ids)[0]
            )
            binary_node.text = base64.b64encode(pdf_inv)

    def _ubl_add_legal_monetary_total(self, parent_node, ns, version="2.1"):
        self.ensure_one()
        monetary_total = etree.SubElement(parent_node, ns["cac"] + "LegalMonetaryTotal")
        cur_name = self.currency_id.name
        prec = self.currency_id.decimal_places
        line_total = etree.SubElement(
            monetary_total, ns["cbc"] + "LineExtensionAmount", currencyID=cur_name
        )
        line_total.text = "%0.*f" % (prec, self.amount_untaxed)
        tax_excl_total = etree.SubElement(
            monetary_total, ns["cbc"] + "TaxExclusiveAmount", currencyID=cur_name
        )
        tax_excl_total.text = "%0.*f" % (prec, self.amount_untaxed)
        tax_incl_total = etree.SubElement(
            monetary_total, ns["cbc"] + "TaxInclusiveAmount", currencyID=cur_name
        )
        tax_incl_total.text = "%0.*f" % (prec, self.amount_total)
        prepaid_amount = etree.SubElement(
            monetary_total, ns["cbc"] + "PrepaidAmount", currencyID=cur_name
        )
        prepaid_value = self.amount_total - self.amount_residual
        prepaid_amount.text = "%0.*f" % (prec, prepaid_value)
        payable_amount = etree.SubElement(
            monetary_total, ns["cbc"] + "PayableAmount", currencyID=cur_name
        )
        payable_amount.text = "%0.*f" % (prec, self.amount_residual)

    def _ubl_add_invoice_line(self, parent_node, iline, line_number, ns, version="2.1"):
        self.ensure_one()
        cur_name = self.currency_id.name
        line_root = etree.SubElement(parent_node, ns["cac"] + "InvoiceLine")
        dpo = self.env["decimal.precision"]
        qty_precision = dpo.precision_get("Product Unit of Measure")
        price_precision = dpo.precision_get("Product Price")
        account_precision = self.currency_id.decimal_places
        line_id = etree.SubElement(line_root, ns["cbc"] + "ID")
        line_id.text = str(line_number)
        uom_unece_code = False
        # product_uom_id is not a required field on account.move.line
        if iline.product_uom_id.unece_code:
            uom_unece_code = iline.product_uom_id.unece_code
            quantity = etree.SubElement(
                line_root, ns["cbc"] + "InvoicedQuantity", unitCode=uom_unece_code
            )
        else:
            quantity = etree.SubElement(line_root, ns["cbc"] + "InvoicedQuantity")
        qty = iline.quantity
        quantity.text = "%0.*f" % (qty_precision, qty)
        line_amount = etree.SubElement(
            line_root, ns["cbc"] + "LineExtensionAmount", currencyID=cur_name
        )
        line_amount.text = "%0.*f" % (account_precision, iline.price_subtotal)
        self._ubl_add_invoice_line_tax_total(iline, line_root, ns, version=version)
        self._ubl_add_item(
            iline.name,
            iline.product_id,
            line_root,
            ns,
            type_="sale",
            taxes=iline.tax_ids,
            version=version,
        )
        price_node = etree.SubElement(line_root, ns["cac"] + "Price")
        price_amount = etree.SubElement(
            price_node, ns["cbc"] + "PriceAmount", currencyID=cur_name
        )
        price_unit = 0.0
        # Use price_subtotal/qty to compute price_unit to be sure
        # to get a *tax_excluded* price unit
        if not float_is_zero(qty, precision_digits=qty_precision):
            price_unit = float_round(
                iline.price_subtotal / float(qty), precision_digits=price_precision
            )
        price_amount.text = "%0.*f" % (price_precision, price_unit)
        if uom_unece_code:
            base_qty = etree.SubElement(
                price_node, ns["cbc"] + "BaseQuantity", unitCode=uom_unece_code
            )
        else:
            base_qty = etree.SubElement(price_node, ns["cbc"] + "BaseQuantity")
        base_qty.text = "%0.*f" % (qty_precision, 1.0)

    def _ubl_add_invoice_line_tax_total(self, iline, parent_node, ns, version="2.1"):
        self.ensure_one()
        cur_name = self.currency_id.name
        prec = self.currency_id.decimal_places
        tax_total_node = etree.SubElement(parent_node, ns["cac"] + "TaxTotal")
        price = iline.price_unit * (1 - (iline.discount or 0.0) / 100.0)
        res_taxes = iline.tax_ids.compute_all(
            price,
            quantity=iline.quantity,
            product=iline.product_id,
            partner=self.partner_id,
        )
        tax_total = float_round(
            res_taxes["total_included"] - res_taxes["total_excluded"],
            precision_digits=prec,
        )
        tax_amount_node = etree.SubElement(
            tax_total_node, ns["cbc"] + "TaxAmount", currencyID=cur_name
        )
        tax_amount_node.text = "%0.*f" % (prec, tax_total)
        if not float_is_zero(tax_total, precision_digits=prec):
            for res_tax in res_taxes["taxes"]:
                tax = self.env["account.tax"].browse(res_tax["id"])
                # we don't have the base amount in res_tax :-(
                self._ubl_add_tax_subtotal(
                    False,
                    res_tax["amount"],
                    tax,
                    cur_name,
                    tax_total_node,
                    ns,
                    version=version,
                )

    @api.model
    def _get_tax_key_for_group_add_base(self, line):
        return [line.tax_line_id.id]

    def _ubl_add_tax_total(self, xml_root, ns, version="2.1"):
        self.ensure_one()
        cur_name = self.currency_id.name
        tax_total_node = etree.SubElement(xml_root, ns["cac"] + "TaxTotal")
        tax_amount_node = etree.SubElement(
            tax_total_node, ns["cbc"] + "TaxAmount", currencyID=cur_name
        )
        prec = self.currency_id.decimal_places
        tax_amount_node.text = "%0.*f" % (prec, self.amount_tax)
        if not float_is_zero(self.amount_tax, precision_digits=prec):

            tax_lines = self.line_ids.filtered(lambda line: line.tax_line_id)
            res = {}
            # There are as many tax line as there are repartition lines
            done_taxes = set()
            for line in tax_lines:
                res.setdefault(
                    line.tax_line_id.tax_group_id,
                    {"base": 0.0, "amount": 0.0, "tax": False},
                )
                res[line.tax_line_id.tax_group_id]["amount"] += line.price_subtotal
                tax_key_add_base = tuple(self._get_tax_key_for_group_add_base(line))
                if tax_key_add_base not in done_taxes:
                    res[line.tax_line_id.tax_group_id]["base"] += line.tax_base_amount
                    res[line.tax_line_id.tax_group_id]["tax"] = line.tax_line_id
                    done_taxes.add(tax_key_add_base)
            res = sorted(res.items(), key=lambda l: l[0].sequence)
            print("res0-----------------------", res)
            for _group, amounts in res:
                self._ubl_add_tax_subtotal(
                    amounts["base"],
                    amounts["amount"],
                    amounts["tax"],
                    cur_name,
                    tax_total_node,
                    ns,
                    version=version,
                )

    def _ubl_get_customer_assigned_id(self, partner):
        return partner.commercial_partner_id.ref

    def _ubl_add_payment_means(
            self,
            partner_bank,
            payment_mode,
            date_due,
            parent_node,
            ns,
            payment_identifier=None,
            version="2.1",
    ):
        pay_means = etree.SubElement(parent_node, ns["cac"] + "PaymentMeans")
        pay_means_code = etree.SubElement(
            pay_means, ns["cbc"] + "PaymentMeansCode", listID="UN/ECE 4461"
        )
        # Why not schemeAgencyID='6' + schemeID
        if payment_mode:  # type is a required field on payment_mode
            if not payment_mode.payment_method_id.unece_id:
                raise UserError(
                    _(
                        "Missing 'UNECE Payment Mean' on payment type '%s' "
                        "used by the payment mode '%s'."
                    )
                    % (payment_mode.payment_method_id.name, payment_mode.name)
                )
            pay_means_code.text = payment_mode.payment_method_id.unece_code
        else:
            pay_means_code.text = "31"
            logger.warning(
                "Missing payment mode on invoice ID %d. "
                "Using 31 (wire transfer) as UNECE code as fallback "
                "for payment mean",
                self.id,
            )
        if date_due:
            pay_due_date = etree.SubElement(pay_means, ns["cbc"] + "PaymentDueDate")
            pay_due_date.text = date_due.strftime("%Y-%m-%d")
        if pay_means_code.text in ["30", "31", "42"]:
            if (
                    not partner_bank
                    and payment_mode
                    and payment_mode.bank_account_link == "fixed"
                    and payment_mode.fixed_journal_id
            ):
                partner_bank = payment_mode.fixed_journal_id.bank_account_id
            if partner_bank and partner_bank.acc_type == "iban":
                # In the Chorus specs, they except 'IBAN' in PaymentChannelCode
                # I don't know if this usage is common or not
                payment_channel_code = etree.SubElement(
                    pay_means, ns["cbc"] + "PaymentChannelCode"
                )
                payment_channel_code.text = "IBAN"
                if payment_identifier:
                    payment_id = etree.SubElement(pay_means, ns["cbc"] + "PaymentID")
                    payment_id.text = payment_identifier
                payee_fin_account = etree.SubElement(
                    pay_means, ns["cac"] + "PayeeFinancialAccount"
                )
                payee_fin_account_id = etree.SubElement(
                    payee_fin_account, ns["cbc"] + "ID", schemeName="IBAN"
                )
                payee_fin_account_id.text = partner_bank.sanitized_acc_number
                if partner_bank.bank_bic:
                    financial_inst_branch = etree.SubElement(
                        payee_fin_account, ns["cac"] + "FinancialInstitutionBranch"
                    )
                    financial_inst = etree.SubElement(
                        financial_inst_branch, ns["cac"] + "FinancialInstitution"
                    )
                    financial_inst_id = etree.SubElement(
                        financial_inst, ns["cbc"] + "ID", schemeName="BIC"
                    )
                    financial_inst_id.text = partner_bank.bank_bic

        # UBL-CR-412: A UBL invoice should not include the PaymentMeans PaymentDueDate
        invoice = parent_node
        payment_means = invoice.find(ns["cac"] + "PaymentMeans")
        if payment_means is not None:
            payment_due_date = payment_means.find(ns["cbc"] + "PaymentDueDate")
            if payment_due_date is not None:
                payment_means.remove(payment_due_date)

            # UBL-CR-413: A UBL invoice should not include the PaymentMeans
            #             PaymentChannelCode
            channel_code = payment_means.find(ns["cbc"] + "PaymentChannelCode")
            if channel_code is not None:
                payment_means.remove(channel_code)

            # UBL-CR-664: A UBL invoice should not include the
            #             FinancialInstitutionBranch FinancialInstitution
            payee_fin_account = payment_means.find(ns["cac"] + "PayeeFinancialAccount")
            if payee_fin_account is not None:
                payee_fin_account_id = payee_fin_account.find(ns["cbc"] + "ID")
                if payee_fin_account_id is not None:
                    payee_fin_account_id.attrib.pop("schemeName", None)
                institution_branch = payee_fin_account.find(
                    ns["cac"] + "FinancialInstitutionBranch"
                )
                if institution_branch is not None:
                    payee_fin_account.remove(institution_branch)

            # UBL-CR-661: A UBL invoice should not include the PaymentMeansCode listID
            payment_means_code = payment_means.find(ns["cbc"] + "PaymentMeansCode")
            if payment_means_code is not None:
                payment_means_code.attrib.pop("listID", None)

    def get_payment_identifier(self):
        """This method is designed to be inherited in localization modules"""
        self.ensure_one()
        return None

    def generate_invoice_ubl_xml_etree(self, version="2.1"):
        self.ensure_one()
        nsmap, ns = self._ubl_get_nsmap_namespace("Invoice-2", version=version)
        xml_root = etree.Element("Invoice", nsmap=nsmap)
        self._ubl_add_header(xml_root, ns, version=version)
        if version == "2.1":
            self._ubl_add_buyer_reference(xml_root, ns, version=version)
        self._ubl_add_order_reference(xml_root, ns, version=version)
        self._ubl_add_contract_document_reference(xml_root, ns, version=version)
        # self._ubl_add_attachments(xml_root, ns, version=version)
        self._ubl_add_supplier_party(
            False,
            self.company_id,
            "AccountingSupplierParty",
            xml_root,
            ns,
            version=version,
        )
        self._ubl_add_customer_party(
            self.partner_id,
            False,
            "AccountingCustomerParty",
            xml_root,
            ns,
            version=version,
        )
        # the field 'partner_shipping_id' is defined in the 'sale' module
        if hasattr(self, "partner_shipping_id") and self.partner_shipping_id:
            self._ubl_add_delivery(self.partner_shipping_id, xml_root, ns)
        # # Put paymentmeans block even when invoice is paid ?
        payment_identifier = self.get_payment_identifier()
        self._ubl_add_payment_means(
            self.payment_mode_id.fixed_journal_id.bank_account_id,
            self.payment_mode_id,
            self.invoice_date_due,
            xml_root,
            ns,
            payment_identifier=payment_identifier,
            version=version,
        )
        if self.invoice_payment_term_id:
            self._ubl_add_payment_terms(
                self.invoice_payment_term_id, xml_root, ns, version=version
            )
        self._ubl_add_tax_total(xml_root, ns, version=version)
        self._ubl_add_legal_monetary_total(xml_root, ns, version=version)
        #
        line_number = 0
        for iline in self.invoice_line_ids:
            line_number += 1
            self._ubl_add_invoice_line(
                xml_root, iline, line_number, ns, version=version
            )
        return xml_root

    def get_ubl_version(self):
        return self.env.context.get("ubl_version") or "2.1"

    def get_ubl_lang(self):
        self.ensure_one()
        return self.partner_id.lang or "en_US"
