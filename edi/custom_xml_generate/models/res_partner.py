from odoo import fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    endpoint_id = fields.Char('EndPoint')
    scheme_id = fields.Char('Scheme')

    def _get_peppol_endpoint_id(self):
        self.ensure_one()
        # Override this function if your business case requires other values.
        return {
            "endpoint_id": self.endpoint_id,
            "scheme_id": self.scheme_id,
        }
